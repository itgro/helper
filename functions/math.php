<?php

use Itgro\Helpers\MathHelpers;

function between ($num, $min, $max = null)
{
	return MathHelpers::between($num, $min, $max);
}
