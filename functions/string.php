<?php

use Itgro\Helpers\StringHelpers;
use Itgro\IO;

/**
 * @param string $format 		Необходимый формат даты
 * @param integer|null $sec		Метка времени (null = текущее время)
 * @param integer $round		Округление
 * @return string
 */
function time_format($format, $sec = null, $round = 0)
{
	return StringHelpers::timeFormat($format, $sec, $round);
}

/**
 * @param mixed $num	Число, на основании которого формируется окончание
 * @param string $e1	Склонение для числа 1 (ex: штука)
 * @param string $e3	Склонение для числа 3 (ex: штуки)
 * @param string $e5	Склонение для числа 5 (ex: штук)
 *
 * @return string
 */
function word_ending($num, $e1, $e3, $e5)
{
	return StringHelpers::wordEnding($num, $e1, $e3, $e5);
}

/**
 * @param $val
 * @return bool
 */
function yes ($val)
{
	return StringHelpers::yes($val);
}

/**
 * @param $val
 * @return bool
 */
function no ($val)
{
	return StringHelpers::no($val);
}

/**
 * @param $string
 * @return mixed|string
 */
function camel ($string)
{
	return StringHelpers::camel($string);
}

/**
 * @param $string
 * @return string
 */
function snake ($string)
{
	return StringHelpers::snake($string);
}

/**
 * @param $string
 * @return string
 */
function lower ($string)
{
	return StringHelpers::lower($string);
}

/**
 * @param $string
 * @return string
 */
function upper ($string)
{
	return StringHelpers::upper($string);
}

/**
 * @param $string
 * @return string
 */
function capitalize ($string)
{
	return StringHelpers::capitalize($string);
}

/**
 * @param mixed $var
 * @param mixed $var,... [optional]
 * @return string
 */
function serialize_all ()
{
	return call_user_func_array(
		['Itgro\Helpers\StringHelpers', 'serializeAll'],
		func_get_args()
	);
}
