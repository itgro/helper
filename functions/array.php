<?php
use Itgro\Helpers\ArrayHelpers;

/**
 * @param array $array
 * @param $regex
 * @return array
 */
function array_key_match (array $array, $regex)
{
	return ArrayHelpers::keyMatch($array, $regex);
}

/**
 * @param array $array
 * @param $keys
 * @return array
 */
function array_only (array $array, $keys)
{
	return ArrayHelpers::only($array, $keys);
}

/**
 * @param array $array
 * @param $keys
 * @return array
 */
function array_exclude (array $array, $keys)
{
	return ArrayHelpers::exclude($array, $keys);
}

/**
 * @param array $array
 * @param $key
 * @return array
 */
function array_pluck ($array, $key)
{
    return ArrayHelpers::pluck($array, $key);
}


/**
 * @param $array
 * @param $key
 * @return array
 */
function array_assoc ($array, $key)
{
	return ArrayHelpers::assoc($array, $key);
}

/**
 * @deprecated use array_filter
 * @param array $array
 * @param callable $callback
 * @return array
 */
function array_find (array $array, Closure $callback, $flag = 0)
{
	return ArrayHelpers::find($array, $callback, $flag);
}


/**
 * @param $array
 * @return array
 */
function array_keys_lower(array $array)
{
	return ArrayHelpers::keysLower($array);
}

/**
 * @param array $array
 * @return array
 */
function array_capitalize(array $array)
{
	return ArrayHelpers::capitalize($array);
}
