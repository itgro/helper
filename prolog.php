<?

use Itgro\App;
use Itgro\IO;
use Symfony\Component\VarDumper\VarDumper;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;

require_once('functions/array.php');
require_once('functions/string.php');
require_once('functions/math.php');

if (PHP_SAPI == 'cli')
{
	require_once('functions/cli.php');
}

$autoloadFile = $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

if (file_exists($autoloadFile))
{
	require_once($autoloadFile);
}

VarDumper::setHandler(function ()
{
    $args = func_get_args();

    if (empty($args))
    {
        return;
    }

    $depth = COption::GetOptionInt('helper', 'dump_depth', 128);
    $children = COption::GetOptionInt('helper', 'dump_children', 128);
    $length = COption::GetOptionInt('helper', 'dump_length', 512);

    $use_xdebug = COption::GetOptionInt('helper', 'use_xdebug', 0);
    $xdebug_loaded = extension_loaded('xdebug');
    $var_dump_overloaded = ini_get('xdebug.overload_var_dump') > 0;

    $output = App::development() ? null : IO::path(IO::root() . '/logs/dump.log');

    if ($use_xdebug && $xdebug_loaded && $var_dump_overloaded)
    {
        $dumper = 'cli' === PHP_SAPI || !App::development() ? 'print_r' : 'var_dump';

        ob_start();
        foreach ($args as $var)
            call_user_func_array($dumper, [$var]);
        $content = ob_get_contents();
        ob_end_clean();

        $handle = fopen($output ?: 'php://output', 'wb');
        fwrite($handle, $content);
    }
    else
    {
        $cloner = new VarCloner();

        $dumper = 'cli' === PHP_SAPI || !App::development() ? new CliDumper() : new Itgro\HtmlDumper();

        $cloner->setMaxString($length);

        foreach ($args as $var)
        {
            $dumper->dump($cloner->cloneVar($var)->withMaxDepth($depth)->withMaxItemsPerDepth($children), $output);
        }
    }
});
