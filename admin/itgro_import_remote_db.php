<?
use Bitrix\Main\Web\HttpClient;
use Bitrix\Main\Web\Uri;
use Itgro\App;
use Itgro\Form;
use Itgro\Request\Request;

define('NO_AGENT_CHECK', true);
define("STATISTIC_SKIP_ACTIVITY_CHECK", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

if (App::ajax())
{
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_js.php");
}
else
{
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin.php");
}

if (Request::is_post() && Request::post()->has('Update'))
{
	if (Request::post()->has('remote'))
	{
		$remote = Request::post()->get('remote');

		COption::SetOptionString('helper', 'db_import_remote', $remote);
	}

	if (Request::post()->has('login'))
	{
		$login = Request::post()->get('login');

		COption::SetOptionString('helper', 'db_import_login', $login);
	}

	$password = Request::post()->get('password');

	if ($remote && $login && $password)
	{
		$url = new URI($remote);

		$url = $url->getScheme() . '://' . $url->getHost() . '/bitrix/tools/helper/export_db.php';

		$client = new HTTPClient();

		$client->setAuthorization($login, $password);

		$result = $client->post($url, [
			'action' => 'start'
		]);

		if ($result = json_decode($result))
		{
			$tablesCount = $viewCount = 0;

			foreach ($result as $table)
			{
				if ($table->type == 'BASE TABLE')
				{
					$tablesCount++;
				}
				else if ($table->type == 'VIEW')
				{
					$viewCount++;
				}
			}

			echo '<div id="result">';


			CAdminMessage::ShowMessage(array(
				"TYPE" => "PROGRESS",
				"MESSAGE" => 'Прогресс',
				"DETAILS" => 'Таблица 1 из '.'#PROGRESS_BAR#',//.
//				GetMessage('TIME_SPENT').' '.HumanTime($NS["time"]),
//		GetMessage('TIME_SPENT').' <span id="counter_field">'.sprintf('%02d',floor($NS["time"]/60)).':'.sprintf('%02d', $NS['time']%60).'</span><!--'.intval($NS['time']).'-->',
				"HTML" => true,
				"PROGRESS_TOTAL" => 100,
				"PROGRESS_VALUE" => 0,
			));

			echo '</div>';
		}
	}
}

$remote = COption::GetOptionString('helper', 'db_import_remote', '');
$login = COption::GetOptionString('helper', 'db_import_login', 'admin');

$aTabs = [
	[
		"DIV" => "main",
		"TAB" => "Импорт",
		"ICON" => "",
		"TITLE" => "Импорт данных"
	]
];

$mid = Request::get()->get('mid');

Form::start(App::getInstance()->GetCurPage().'?mid='.htmlspecialcharsbx($mid).'&lang='.LANGUAGE_ID);

$tabControl = new CAdminTabControl("tabControl", $aTabs);
$tabControl->Begin();

$tabControl->BeginNextTab();
?>
	<tr>
		<td width="40%" class="adm-detail-content-cell-l">
			Адрес удаленного сайта:
		</td>
		<td width="60%" class="adm-detail-content-cell-r">
			<?
			Form::input(
				'text',
				'remote',
				$remote,
				''
			);?>
		</td>
	</tr>
	<tr>
		<td width="40%" class="adm-detail-content-cell-l">
			Логин администратора:
		</td>
		<td width="60%" class="adm-detail-content-cell-r">
			<?
			Form::input(
				'text',
				'login',
				$login,
				''
			);?>
		</td>
	</tr>
	<tr>
		<td width="40%" class="adm-detail-content-cell-l">
			Пароль администратора:
		</td>
		<td width="60%" class="adm-detail-content-cell-r">
			<?
			Form::input(
				'password',
				'password',
				$password,
				'Lkoiasdlkjf678)'
			);?>
		</td>
	</tr>
<?
$tabControl->EndTab();

$tabControl->Buttons();

Form::input('submit', 'start_button', 'Старт', null, ['class' => 'adm-btn-save']);
Form::input('hidden', 'Update', 'Y');
Form::input('button', 'stop_button', 'Стоп', null, ['disabled' => 'disabled']);

$tabControl->End();

Form::end();
