<?

CModule::AddAutoloadClasses(
	'helper',
	array(
		#region Main
		'Itgro\App' => 'lib/app.php',
		'Itgro\Cli' => 'lib/cli.php',
		'Itgro\Http' => 'lib/http.php',
		'Itgro\User' => 'lib/user.php',
		'Itgro\Html' => 'lib/html.php',
		'Itgro\Form' => 'lib/form.php',
		'Itgro\IO' => 'lib/io.php',
		'Itgro\Xhprof' => 'lib/xhprof.php',
		'Itgro\Debug' => 'lib/debug.php',
		'Itgro\Cache' => 'lib/cache.php',
		'Itgro\Singleton' => 'lib/singleton.php',
		'Itgro\HtmlDumper' => 'lib/html_dumper.php',
		'Itgro\Twig' => 'lib/twig.php',
		#endregion
		#region Helpers
		'Itgro\Helpers\ArrayHelpers' => 'lib/helpers/array_helpers.php',
		'Itgro\Helpers\CliHelpers' => 'lib/helpers/cli_helpers.php',
		'Itgro\Helpers\MathHelpers' => 'lib/helpers/math_helpers.php',
		'Itgro\Helpers\StringHelpers' => 'lib/helpers/string_helpers.php',
		#endregion
		#region Request
		'Itgro\Request\Request' => 'lib/request/request.php',
		'Itgro\Request\Method\Base' => 'lib/request/method/base.php',
		'Itgro\Request\Method\File' => 'lib/request/method/file.php',
		'Itgro\Request\Method\Get' => 'lib/request/method/get.php',
		'Itgro\Request\Method\Post' => 'lib/request/method/post.php',
		'Itgro\Request\Method\Put' => 'lib/request/method/put.php',
		'Itgro\Request\Method\Request' => 'lib/request/method/request.php',
		#endregion
		#region Exception
		'Itgro\Exception\BaseException' => 'lib/exception/base.php',
		'Itgro\Exception\ModuleNotIncludedException' => 'lib/exception/module_not_installed.php',
		#endregion
		#region Collection
		'Itgro\Collection\Base' => 'lib/collection/base.php',
		'Itgro\Collection\ArrayCollection' => 'lib/collection/array.php',
		'Itgro\Collection\DBCollection' => 'lib/collection/db.php',
		#endregion
	)
);

require_once('prolog.php');
