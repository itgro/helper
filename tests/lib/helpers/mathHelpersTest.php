<?php

namespace Itgro\Helpers\Tests;

use Itgro\Helpers\MathHelpers;

class MathHelpersTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @dataProvider betweenProvider
	 */
	public function testBetween($num, $min, $max, $bool = true)
	{
		$this->assertEquals(
			MathHelpers::between($num, $min, $max),
			$bool
		);
	}

	public function betweenProvider ()
	{
		return [
			[2, 1, 2, true],
			[2, 2, 3, true],
			[2, [2, 3], null, true],
			[2, [1, 2], null, true],
			[2, [3, 1], null, true],
			[1, [2, 4], null, false],
			[1, [4, 2], null, false],
		];
	}
}
