<?php

namespace Itgro\Helpers\Tests;

use Itgro\Helpers\StringHelpers;

class StringHelpersTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @dataProvider timeFormatProvider
	 */
	public function testTimeFormat($result, $format, $sec, $round = 0)
	{
		$this->assertEquals(
			StringHelpers::timeFormat($format, $sec, $round),
			$result
		);
	}

	public function timeFormatProvider ()
	{
		return [
			['15:15:15', 'H:M:S', 60 * 60 * 15 + 60 * 15 + 15],
			['15:15:00', 'H:M:S', 60 * 60 * 15 + 60 * 20 + 15, 900],
			['15:20:00', 'H:M:S', 60 * 60 * 15 + 60 * 20 + 15, 600],
			['15.25', 'HH', 60 * 60 * 15 + 60 * 15],
			['05.25', 'HH', 60 * 60 * 5 + 60 * 15],
			['915.25', 'MM', 60 * 60 * 15 + 60 * 15 + 15],
			['54915.00', 'SS', 60 * 60 * 15 + 60 * 15 + 15],
			['5.25', 'hh', 60 * 60 * 5 + 60 * 15 + 15],
			['5.25', 'mm', 60 * 5 + 15],
			['5.00', 'ss', 5],
		];
	}

	public function testWordEnding()
	{

	}

	public function testYes()
	{

	}

	public function testNo()
	{

	}

	public function testCamel()

	{
	}

	public function testSnake()
	{

	}

	public function testLower()
	{

	}

	public function testUpper()
	{

	}

	public function testCapitalize()
	{

	}

	public function testSerializeAll()
	{

	}
}
