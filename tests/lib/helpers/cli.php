<?php

namespace Itgro\Helpers;

use Itgro\Cli;

class CliHelpers
{
	/**
	 * @param mixed $line
	 * @param mixed $line,... [optional]
	 */
	public static function printLn($line, $line = null)
	{
		$args = func_get_args();

		foreach ($args as &$var)
		{
			Cli::println($var);
		}
		return;
	}

	public static function cliException(\Exception $e)
	{
		$className = get_class($e);
		$title = "Uncaught $className";
		$message = $e->getMessage();

		println("#RED#$title:#NORMAL# #BOLD#$message");

		$lineNum = 1;

		foreach ($e->getTrace() as $stackLine)
		{
			if (array_key_exists('file', $stackLine))
			{
				$line = "\t$lineNum. file: " . str_replace($_SERVER['DOCUMENT_ROOT'], '', $stackLine['file']);

				if (array_key_exists('line', $stackLine))
				{
					$line .= ":$stackLine[line]";
				}

				static::printLn($line);

				$lineNum++;
			}
		}
	}

	public static function cliError($title, $message = null, $code = null)
	{
		if ($message == null)
		{
			$message = $title;
			$title = 'Ошибка';
		}

		if ($code !== null)
			$title .= ' #' . $code;

		static::printLn("#RED#$title:#NORMAL# #BOLD#$message");

		foreach (debug_backtrace() as $stackLine)
		{
			if (array_key_exists('file', $stackLine))
			{
				$line = "\t1. file: " . str_replace($_SERVER['DOCUMENT_ROOT'], '', $stackLine['file']);

				if (array_key_exists('line', $stackLine))
				{
					$line .= ":$stackLine[line]";
				}

				static::printLn($line);
			}
		}
	}
}
