<?php

namespace Itgro\Helpers\Tests;

use Itgro\Helpers\ArrayHelpers;

class ArrayHelpersTest extends \PHPUnit_Framework_TestCase
{
	public function testKeyMatch ()
	{
		$testArray = [
			'simpleElement' => 'simpleElement',
			'simpleRegex_1' => '1',
			'simpleRegex_2' => '2',
		];

		$this->assertEquals(
			ArrayHelpers::keyMatch($testArray, '/simpleElement/uis'),
			[
				'simpleElement' => 'simpleElement',
			]
		);

		$this->assertEquals(
			ArrayHelpers::keyMatch($testArray, '/simpleRegex_[\d]/uis'),
			[
				'simpleRegex_1' => '1',
				'simpleRegex_2' => '2',
			]
		);

		$this->assertEquals(
			ArrayHelpers::keyMatch($testArray, '/simpleelement/us'),
			[]
		);

		$this->assertEquals(
			ArrayHelpers::keyMatch($testArray, '/simpleelement/uis'),
			[
				'simpleElement' => 'simpleElement',
			]
		);
	}

	public function testOnly ()
	{
		$testArray = [
			'simpleElement' => 'simpleElement',
			'simpleRegex_1' => '1',
			'simpleRegex_2' => '2',
		];

		$this->assertEquals(
			ArrayHelpers::only($testArray, ['simpleElement']),
			[
				'simpleElement' => 'simpleElement',
			]
		);

		$this->assertEquals(
			ArrayHelpers::only($testArray, []),
			[]
		);

		$this->assertEquals(
			ArrayHelpers::only($testArray, ['undefinedIndex']),
			[]
		);
	}

	public function testExclude ()
	{
		$testArray = [
			'simpleElement' => 'simpleElement',
			'simpleRegex_1' => '1',
			'simpleRegex_2' => '2',
		];

		$this->assertEquals(
			ArrayHelpers::exclude($testArray, ['simpleElement']),
			[
				'simpleRegex_1' => '1',
				'simpleRegex_2' => '2',
			]
		);

		$this->assertEquals(
			ArrayHelpers::exclude($testArray, []),
			$testArray
		);

		$this->assertEquals(
			ArrayHelpers::exclude($testArray, ['undefinedIndex']),
			$testArray
		);
	}

	public function testPluck ()
	{
		$testArray = [
			[
				'id' => 1,
				'name' => 'one',
				'attributes' => [
					'class' => 'class 1',
					'data' => 'data 1'
				]
			],
			[
				'id' => 2,
				'name' => 'two',
				'attributes' => [
					'class' => 'class 2',
					'data' => 'data 2'
				]
			],
			[
				'id' => 3,
				'name' => 'three',
				'attributes' => [
					'class' => 'class 3',
					'data' => 'data 3'
				]
			],
		];

		$this->assertEquals(
			ArrayHelpers::pluck($testArray, 'id'),
			[1, 2, 3]
		);

		$this->assertEquals(
			ArrayHelpers::pluck($testArray, 'name'),
			['one', 'two', 'three']
		);

		$this->assertEquals(
			ArrayHelpers::pluck($testArray, 'attributes.data'),
			['data 1', 'data 2', 'data 3']
		);

		$testObject = new \StdClass();
		$testObject->items = [];

		foreach (range(0, 5) as $i)
		{
			$testObject->items[$i] = new \StdClass();
			$testObject->items[$i]->id = $i;
			$testObject->items[$i]->name =  new \StdClass();
			$testObject->items[$i]->name->first_name = 'first_' . $i;
			$testObject->items[$i]->name->last_name = 'last_' . $i;
		}

		$this->assertEquals(
			ArrayHelpers::pluck($testObject->items, 'id'),
			[0, 1, 2, 3, 4, 5]
		);

		$this->assertEquals(
			ArrayHelpers::pluck($testObject->items, 'name.first_name'),
			['first_0', 'first_1', 'first_2', 'first_3', 'first_4', 'first_5']
		);

		$this->assertEquals(
			ArrayHelpers::pluck($testObject->items, 'name.last_name'),
			['last_0', 'last_1', 'last_2', 'last_3', 'last_4', 'last_5']
		);
	}

	public function testAssoc ()
	{
		$testArray = [
			[
				'id' => 1,
				'name' => 'one',
				'email' => 'one@mail.ru',
			],
			[
				'id' => 2,
				'name' => 'two',
				'email' => 'two@mail.ru',
			],
			[
				'id' => 3,
				'name' => 'three',
			],
		];

		$assertArray = [
			1 => $testArray[0],
			2 => $testArray[1],
			3 => $testArray[2],
		];

		$this->assertEquals(
			ArrayHelpers::assoc($testArray, 'id'),
			$assertArray
		);

		$assertArray = [
			'one' => $testArray[0],
			'two' => $testArray[1],
			'three' => $testArray[2],
		];

		$this->assertEquals(
			ArrayHelpers::assoc($testArray, 'name'),
			$assertArray
		);

		$assertArray = [
			'one@mail.ru' => $testArray[0],
			'two@mail.ru' => $testArray[1],
			'' => $testArray[2],
		];

		$this->assertEquals(
			ArrayHelpers::assoc($testArray, 'email'),
			$assertArray
		);
	}

	public function testKeysLower()
	{
		$testArray = [
			[
				'ID' => 1,
				'NAME' => 'one',
				'ATTRIBUTES' => [
					'CLASS' => 'class 1',
					'DATA' => [
						'DATA_1' => 1,
						'DATA_2' => 1,
					]
				]
			]
		];

		$assertArray = [
			[
				'id' => 1,
				'name' => 'one',
				'attributes' => [
					'class' => 'class 1',
					'data' => [
						'data_1' => 1,
						'data_2' => 1,
					]
				]
			]
		];

		$this->assertEquals(
			ArrayHelpers::keysLower($testArray),
			$assertArray
		);
	}

	public function testCapitalize()
	{
		$testArray = [
			[
				'id' => 1,
				' name' => 'ONE',
				'attributes' => [
					' CLASS ' => ' CLASS 1 ',
					'data' => [
						' data_1 ' => 1,
						' data_2 ' => 1,
					]
				]
			]
		];

		$assertArray = [
			[
				'id' => 1,
				'name' => 'one',
				'attributes' => [
					'class' => 'class 1',
					'data' => [
						'data_1' => 1,
						'data_2' => 1,
					]
				]
			]
		];

		$this->assertEquals(
			ArrayHelpers::capitalize($testArray),
			$assertArray
		);
	}

}
