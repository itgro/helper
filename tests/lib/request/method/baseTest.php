<?php

namespace Itgro\Tests\Request\Method;

use Itgro\Request\Method\Request;

class BaseTest extends \PHPUnit_Framework_TestCase
{
	public function testExplodeParamArray ()
	{
		$method = \Itgro\Request\Request::getMethod('get');

		$this->assertEquals(
			$method->explodeParamArray('param1[param2]skiped[param3]'),
			['param1', 'param2']
		);

		$this->assertEquals(
			$method->explodeParamArray('[param1][param2]'),
			[]
		);

		$this->assertEquals(
			$method->explodeParamArray('param1[][][]'),
			[
				'param1',
				0,
				0,
				0
			]
		);

		$this->assertEquals(
			$method->explodeParamArray('param1'),
			[
				'param1',
			]
		);
	}
}
