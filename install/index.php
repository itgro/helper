<?php
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (class_exists('helper'))
	return;

Class helper extends CModule
{
	var $MODULE_ID = 'helper';
	var $MODULE_SORT;
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $PARTNER_NAME;
	var $PARTNER_URI;
	var $MODULE_DIR;

	public function __construct()
	{
        /** @var array $arModuleVersion */
        include('version.php');
		$this->MODULE_SORT = 1;
		$this->MODULE_VERSION = $arModuleVersion['VERSION'];
		$this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
		$this->MODULE_NAME = GetMessage("ITGRO_HELPER_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("ITGRO_HELPER_MODULE_DESC");
		$this->PARTNER_NAME = GetMessage("ITGRO_HELPER_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("ITGRO_HELPER_PARTNER_URI");
		$this->MODULE_DIR = dirname(__FILE__)."/../";
	}

	public function InstallFiles($arParams = array())
	{
		return true;
	}

	public function UnInstallFiles()
	{
		return true;
	}

	public function InstallDB()
	{
		RegisterModuleDependences("main", "OnBeforeProlog", $this->MODULE_ID, "Itgro\Xhprof", "start", 1);
		RegisterModuleDependences("main", "OnEpilog", $this->MODULE_ID, "Itgro\Xhprof", "stop", 1);

        return true;
	}

	public function UninstallDB()
	{
		UnRegisterModuleDependences("main", "OnBeforeProlog", $this->MODULE_ID, "CModule", "IncludeModule", 2, '', [$this->MODULE_ID]);
		UnRegisterModuleDependences("main", "OnBeforeProlog", $this->MODULE_ID, "Itgro\Xhprof", "start", 1);
		UnRegisterModuleDependences("main", "OnEpilog", $this->MODULE_ID, "Itgro\Xhprof", "stop", 1);

        return true;
	}

	public function DoInstall()
	{
        if (!$this->InstallFiles())
            return false;

        if (!$this->InstallDB())
            return false;

		RegisterModule($this->MODULE_ID);

        return true;
	}

	public function DoUninstall()
	{
        if (!$this->UnInstallFiles())
            return false;

        if (!$this->UninstallDB())
            return false;

		UnRegisterModule($this->MODULE_ID);

        return true;
	}
}
