<?php
namespace Itgro;

/**
 * Class Cli
 * @package Itgro
 */
class Cli
{
	#region Дополнительные свойства для текта:
	const CLI_BOLD 			= "\033[1m";	#  жирный шрифт (интенсивный цвет)
	const CLI_DBOLD			= "\033[2m";	#  полу яркий цвет (тёмно-серый, независимо от цвета)
	const CLI_NBOLD			= "\033[22m";	#  установить нормальную интенсивность
	const CLI_UNDERLINE		= "\033[4m";	#  подчеркивание
	const CLI_NUNDERLINE	= "\033[4m";	#  отменить подчеркивание
	const CLI_BLINK			= "\033[5m";	#  мигающий
	const CLI_NBLINK		= "\033[5m";	#  отменить мигание
	const CLI_INVERSE		= "\033[7m";	#  реверсия (знаки приобретают цвет фона, а фон -- цвет знаков)
	const CLI_NINVERSE		= "\033[7m";	#  отменить реверсию
	const CLI_BR			= "\033[m";		#  все атрибуты по умолчанию
	const CLI_NORMAL		= "\033[0m";    #  все атрибуты по умолчанию
	#endregion

	#region Цвет текста:
	const CLI_BLACK			= "\033[0;30m";	#  чёрный цвет знаков
	const CLI_RED			= "\033[0;31m";	#  красный цвет знаков
	const CLI_GREEN			= "\033[0;32m";	#  зелёный цвет знаков
	const CLI_YELLOW		= "\033[0;33m";	#  желтый цвет знаков
	const CLI_BLUE			= "\033[0;34m";	#  синий цвет знаков
	const CLI_MAGENTA		= "\033[0;35m";	#  фиолетовый цвет знаков
	const CLI_CYAN			= "\033[0;36m";	#  цвет морской волны знаков
	const CLI_GRAY			= "\033[0;37m";	#  серый цвет знаков
	#endregion

	#region Цветом текста (жирным) (bold) :
	const CLI_DEF			= "\033[0;39m";
	const CLI_DGRAY			= "\033[1;30m";
	const CLI_LRED			= "\033[1;31m";
	const CLI_LGREEN		= "\033[1;32m";
	const CLI_LYELLOW		= "\033[1;33m";
	const CLI_LBLUE			= "\033[1;34m";
	const CLI_LMAGENTA		= "\033[1;35m";
	const CLI_LCYAN			= "\033[1;36m";
	const CLI_WHITE			= "\033[1;37m";
	#endregion

	#region Цвет фона
	const CLI_BGBLACK		= "\033[40m";
	const CLI_BGRED			= "\033[41m";
	const CLI_BGGREEN		= "\033[42m";
	const CLI_BGBROWN		= "\033[43m";
	const CLI_BGBLUE		= "\033[44m";
	const CLI_BGMAGENTA		= "\033[45m";
	const CLI_BGCYAN		= "\033[46m";
	const CLI_BGGRAY		= "\033[47m";
	//const CLI_BGDEF			= "\033[49m";
	#endregion

	static function parseArgs (&$argv)
	{
		$tmp = $argv;
		foreach ($tmp as $k => &$v)
			$v = str_replace('&', '&amp;', $v);

		$queryString = implode('&', $tmp);
		parse_str($queryString, $tmp);

		$argv = [];

		foreach ($tmp as $k => $v)
		{
			$v = urldecode($v);
			$k = urldecode($k);

			if (strlen($v) < 1)
			{
				$argv[] = $k;
			}
			else
			{
				$argv[$k] = $v;
			}
		}
	}

	static function println ($line)
	{
		echo static::colorize($line) . static::CLI_NORMAL . PHP_EOL;
	}

	static function colorize ($string)
	{
		$date = date('H:i:s');

		return str_replace(
			[
				'#TIME#',

				'#NORMAL#',
				'#BOLD#',

				'#BLACK#',
				'#RED#',
				'#GREEN#',
				'#YELLOW#',
				'#BLUE#',
				'#MAGENTA#',
				'#CYAN#',
				'#GRAY#',

				'#BGBLACK#',
				'#BGRED#',
				'#BGGREEN#',
				'#BGBROWN#',
				'#BGBLUE#',
				'#BGMAGENTA#',
				'#BGCYAN#',
				'#BGGRAY#',
			],
			[
				$date,

				static::CLI_NORMAL,
				static::CLI_BOLD,

				static::CLI_BLACK,
				static::CLI_RED,
				static::CLI_GREEN,
				static::CLI_YELLOW,
				static::CLI_BLUE,
				static::CLI_MAGENTA,
				static::CLI_CYAN,
				static::CLI_GRAY,

				static::CLI_BGBLACK,
				static::CLI_BGRED,
				static::CLI_BGGREEN,
				static::CLI_BGBROWN,
				static::CLI_BGBLUE,
				static::CLI_BGMAGENTA,
				static::CLI_BGCYAN,
				static::CLI_BGGRAY,
			],
			$string
		);
	}
}


