<?php
namespace Itgro;

use Bitrix\Main\Application;
use CMain;
use COption;
use Exception;

/**
 * Class App extend CMain
 * @package Itgro
 *
 * @method static string __GetConditionFName
 * @method static void FileAction
 * @method static void reinitPath
 * @method static string getCurPage (bool $get_index_page = null)
 * @method static void setCurPage (string $page, bool $param = false)
 * @method static string GetCurUri (string $addParam = "", bool $get_index_page = null)
 * @method static string GetCurPageParam (string $strParam = "", array $arParamKill = [], bool $get_index_page = null)
 * @method static string GetCurParam
 * @method static string GetCurDir
 * @method static string GetFileRecursive (string $strFileName, string $strDir = false)
 * @method static void IncludeAdminFile (string $strTitle, string $filepath)
 * @method static void SetAuthResult (array $arAuthResult)
 * @method static void AuthForm (string $mess, bool $show_prolog = true, bool $show_epilog = true, string $not_show_links = "N", bool $do_die = true)
 * @method static void ShowAuthForm (string $message)
 * @method static bool NeedCAPTHAForLogin (string $login)
 * @method static string GetMenuHtml (string $type = "left", bool $bMenuExt = false, string $template = false, string $sInitDir = false)
 * @method static string GetMenuHtmlEx (string $type = "left", bool $bMenuExt = false, string $template = false, string $sInitDir = false)
 * @method static string GetMenu (string $type = "left", bool $bMenuExt = false, string $template = false, string $sInitDir = false)
 * @method static bool IsHTTPS
 * @method static string GetTitle (string $property_name = false, bool $strip_tags = false)
 * @method static void SetTitle (string $title, array $arOptions = null)
 * @method static void ShowTitle (string $property_name = "title", bool $strip_tags = true)
 * @method static void SetPageProperty (string $PROPERTY_ID, string $PROPERTY_VALUE, array $arOptions = null)
 * @method static string GetPageProperty (string $PROPERTY_ID, string $default_value = false)
 * @method static void ShowProperty (string $PROPERTY_ID, string $default_value = false)
 * @method static string GetProperty (string $PROPERTY_ID, string $default_value = false)
 * @method static array GetPagePropertyList
 * @method static string InitPathVars (string &$site, string &$path)
 * @method static void SetDirProperty (string $PROPERTY_ID, string $PROPERTY_VALUE, string $path = false)
 * @method static bool InitDirProperties (string $path)
 * @method static string GetDirProperty (string $PROPERTY_ID, string $path = false, string $default_value = false)
 * @method static array GetDirPropertyList (string $path = false)
 * @method static string GetMeta (string $id, string $meta_name = false, bool $bXhtmlStyle = true)
 * @method static void ShowBanner (string $type, string $html_before = "", string $html_after = "")
 * @method static void ShowMeta (string $id, string $meta_name = false, bool $bXhtmlStyle = true)
 * @method static string GetHeadStrings (string $location = \Bitrix\Main\Page\AssetLocation::AFTER_JS_KERNEL)
 * @method static void ShowHeadStrings
 * @method static bool IsExternalLink (string $src)
 * @method static void ShowHeadScripts
 * @method static void ShowBodyScripts
 * @method static void ShowHead (bool $bXhtmlStyle=true)
 * @method static void ShowAjaxHead (bool $bXhtmlStyle = true, bool $showCSS = true, bool $showStrings = true, bool $showScripts = true)
 * @method static void SetShowIncludeAreas (bool $bShow = true)
 * @method static bool GetShowIncludeAreas
 * @method static void SetPublicShowMode (string $mode)
 * @method static string GetPublicShowMode
 * @method static void SetEditArea (string $areaId, array $arIcons)
 * @method static string IncludeStringBefore
 * @method static string IncludeStringAfter (array $arIcons = false, array $arParams = [])
 * @method static string IncludeString (string $string, array $arIcons = false)
 * @method static string GetTemplatePath (string $rel_path)
 * @method static string|bool SetTemplateCSS (string $rel_path)
 * @method static mixed IncludeComponent (string $componentName, string $componentTemplate, array $arParams = [], \CBitrixComponent $parentComponent = false, array $arFunctionParams = [])
 * @method static bool|\CBitrixComponent getCurrentIncludedComponent
 * @method static void AddViewContent (string $view, string $content, int $pos = 500)
 * @method static void ShowViewContent (string $view)
 * @method static string GetViewContent (string $view)
 * @method static void OnChangeFileComponent (string $path, string $site)
 * @method static void IncludeFile (string $rel_path, array $arParams = [], array $arFunctionParams = [])
 * @method static void AddChainItem (string $title, string $link = "", bool $bUnQuote = true)
 * @method static mixed|string GetNavChain (string $path = false, int $iNumFrom = 0, string $sNavChainPath = false, bool $bIncludeOnce = false, bool $bShowIcons = true)
 * @method static void ShowNavChain (string $path = false, int $iNumFrom = 0, string $sNavChainPath = false)
 * @method static void ShowNavChainEx (string $path = false, int $iNumFrom = 0, string $sNavChainPath = false)
 * @method static void SetFileAccessPermission (string $path, array $arPermissions, bool $bOverWrite = true)
 * @method static void RemoveFileAccessPermission (string $path, array $arGroups = false)
 * @method static void CopyFileAccessPermission (string $path_from, string $path_to, bool $bOverWrite = true)
 * @method static string GetFileAccessPermission (string $path, mixed $groups = false, string $task_mode = true)
 * @method static string GetFileAccessPermissionByUser (int $intUserID, string $path, mixed $groups = false, string $task_mode = true)
 * @method static boolean SaveFileContent (string $abs_path, string $strContent)
 * @method static string GetFileContent (string $path)
 * @method static mixed ProcessLPA (string $filesrc = false, $old_filesrc = false)
 * @method static void LPAComponentChecker (array &$arParams, array &$arPHPparams, string $parentParamName)
 * @method static string _ReplaceNonLatin (string $str)
 * @method static array GetLangSwitcherArray ()
 * @method static array GetSiteSwitcherArray ()
 * @method static array GetUserRoles (string $module_id, array $arGroups = false, string $use_default_role = 'Y', string $max_role_for_super_admin = 'Y', string $site_id = false)
 * @method static bool|null|string GetUserRight (string $module_id, array $arGroups = false, string $use_default_role = 'Y', string $max_role_for_super_admin = 'Y', string $site_id = false)
 * @method static array GetUserRightArray (string $module_id, array $arGroups = false)
 * @method static \CDBResult GetGroupRightList (array $arFilter, string $site_id = false)
 * @method static bool|null|string GetGroupRight (string $module_id, array $arGroups = false, string $use_default_role = 'Y', string $max_role_for_super_admin = 'Y', string $site_id = false)
 * @method static void SetGroupRight (string $module_id, int $group_id, string $right, string $site_id = false)
 * @method static void DelGroupRight (string $module_id = '', array $arGroups = [], string $site_id = false)
 * @method static array GetMainRightList ()
 * @method static array GetDefaultRightList ()
 * @method static string err_mess ()
 * @method static boolean get_cookie (string $name, string $name_prefix = false)
 * @method static void set_cookie (string $name, string $value, int $time = false, string $folder = '/', string $domain = false, boolean $secure = false, boolean $spread = true, string $name_prefix = false, boolean $httpOnly = false)
 * @method static string GetCookieDomain ()
 * @method static void StoreCookies ()
 * @method static boolean HoldSpreadCookieHTML ()
 * @method static string GetSpreadCookieHTML ()
 * @method static array GetSpreadCookieUrls ()
 * @method static void ShowSpreadCookieHTML ()
 * @method static void AddPanelButton (array $arButton, boolean $bReplace = false)
 * @method static void AddPanelButtonMenu (string $button_id, array $arMenuItem)
 * @method static void GetPanel ()
 * @method static void ShowPanel ()
 * @method static void PrintHKGlobalUrlVar ()
 * @method static string GetLang (string $cur_dir = false, string $cur_host = false)
 * @method static string GetSiteByDir (string $cur_dir = false, string $cur_host = false)
 * @method static boolean RestartWorkarea (boolean $start = false)
 * @method static void AddBufferContent (callable $callback)
 * @method static string &EndBufferContentMan ()
 * @method static string EndBufferContent (string $content)
 * @method static void ResetException ()
 * @method static void ThrowException (mixed $msg, mixed $id = false)
 * @method static string|\CApplicationException GetException ()
 * @method static string ConvertCharset (string $string, string $charset_in, string $charset_out)
 * @method static array ConvertCharsetArray (array $arData, string $charset_from, string $charset_to)
 * @method static string CaptchaGetCode ()
 * @method static boolean CaptchaCheckCode ()
 * @method static string UnJSEscape (string $str)
 * @method static void ShowFileSelectDialog (string $event, array $arResultDest, array $arPath = [], string $fileFilter = "", boolean $bAllowFolderSelect = false)
 * @method static string GetPopupLink (array $arUrl)
 * @method static string GetServerUniqID ()
 * @method static void PrologActions ()
 * @method static void FinalActions ()
 * @method static void EpilogActions ()
 * @method static void ForkActions ()
 * 29.10.2015
 */
class App
{
	/** @var CMain */
	private static $main;
	private static $env;
	private static $crawler;

	private static $crawlers_whitelist = [
		'googlebot.com' => 'google',
		'yandex.ru' => 'yandex',
		'yandex.net' => 'yandex',
		'yandex.con' => 'yandex',
		'yahoo.net' => 'yahoo',
		'search.msn.net' => 'bing',
	];

	/**
	 * @return CMain
	 */
	public static function getInstance ()
	{
		if (is_null(static::$main))
		{
			static::$main = $GLOBALS['APPLICATION'];
		}

		return static::$main;
	}

	/**
	 * @return string
	 */
	public static function charset ()
	{
		if (defined('BX_UTF') && BX_UTF)
		{
			return 'utf-8';
		}

		return 'cp1251';
	}

	/**
	 * @return string
	 */
	private static function detectEnv ()
	{
		$detect_env = COption::GetOptionInt('helper', 'detect_env', 1);

		if ($detect_env == 2)
		{
			$dev_groups = COption::GetOptionString('helper', 'dev_groups', '["1"]');
			$dev_groups = json_decode($dev_groups, true);

			$groups = User::getInstance()->GetUserGroupArray();

			if (count(array_intersect($dev_groups, $groups)))
			{
				return static::environment('dev');
			}
			else
			{
				return static::environment('production');
			}
		}
		else if ($detect_env == 3)
		{
			return static::environment(COption::GetOptionString('helper', 'environment', 'production'));
		}
		else
		{
			return static::environment(getenv('APP_ENV'));
		}
	}

	/**
	 * @return string
	 */
	public static function environment ($env = null)
	{
		if (null !== $env)
		{
			static::$env = $env;
		}

		if (null === static::$env)
		{
			static::detectEnv();
		}

		return static::$env;
	}

	/**
	 * @return bool
	 */
	public static function development ()
	{
		return in_array(static::environment(), ['dev', 'development']);
	}

	/**
	 * @return bool
	 */
	public static function testing ()
	{
		return in_array(static::environment(), ['test', 'testing']);
	}

	/**
	 * @return bool
	 */
	public static function ajax ()
	{
		$context = Application::getInstance()->getContext();

		if ($context != null)
		{
			$server = $context->getServer();

			if ($server != null)
			{
				return $server->get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest';
			}
		}

		return false;
	}

	/**
	 * @return string|bool
	 */
	public static function robot ()
	{
		if (is_null(static::$crawler))
		{
			static::$crawler = false;

			$context = Application::getInstance()->getContext();

			if ($context != null)
			{
				$server = $context->getServer();

				if ($server != null)
				{
					$domain = $server->get('REMOTE_HOST');

					if (null === $domain)
					{
						$domain = gethostbyaddr($server->get('REMOTE_ADDR'));
					}

					foreach (static::$crawlers_whitelist as $host => $name)
					{
						if (preg_match('/'.preg_quote($host).'$/', $domain))
						{
							static::$crawler = $name;
							break;
						}
					}
				}
			}
		}

		return static::$crawler;
	}

	/**
	 * @return bool
	 */
	public static function isRobot ()
	{
		return self::robot() !== false;
	}

	/**
	 * @static
	 * @param $method
	 * @param $args
	 * @throws \InvalidArgumentException
	 * @return mixed
	 */
	public function __call ($method, $args)
	{
		$app = $this->getInstance();

		if (method_exists($app, $method))
		{
			return call_user_func_array([$app, $method], $args);
		}

		throw new \InvalidArgumentException();
	}

	/**
	 * @static
	 * @param $method
	 * @param $args
	 * @throws \InvalidArgumentException
	 * @return mixed
	 */
	public static function __callStatic ($method, $args)
	{
		$app = new static();

		return call_user_func_array([$app, $method], $args);
	}

	#region Deprecated

	/**
	 * @deprecated use Itgro\Http class
	 */
	public static function clearOutput ()
	{
		static::getInstance()->RestartBuffer();
	}

	/**
	 * @deprecated use Itgro\Http class
	 * @param $code
	 */
	public static function status ($code, $message = '')
	{
		Http::status(trim($code . " " . $message));
	}

	/**
	 * @deprecated use Itgro\Http class
	 * @param $value
	 * @param int $status
	 */
	public static function json_response ($value, $status = Http::HTTP_OK)
	{
		Http::send_json($value, $status);
	}

	/**
	 * @deprecated use Itgro\Http class
	 * @static
	 * @param string $message
	 */
	public static function json_ok ($message = '')
	{
		Http::send_json(['message' => $message]);
	}

	/**
	 * @deprecated use Http class
	 * @static
	 * @param string $message
	 */
	public static function json_error ($message = '')
	{
		Http::send_json(['message' => $message], Http::HTTP_INTERNAL_SERVER_ERROR);
	}

	/**
	 * @deprecated use Http class
	 * @static
	 * @param Exception $e
	 */
	public static function json_exception (Exception $e)
	{
		Http::send_json(['message' => $e->getMessage()], Http::HTTP_INTERNAL_SERVER_ERROR);
	}

	#endregion
}


