<?php
namespace Itgro;

/**
 * @package Itgro
 */
abstract class Singleton
{
	/** @var static[] */
	protected static $instances = [];

	abstract public function __construct();

	public static function getInstance()
	{
		// TODO PHP 5.5 поддерживает константу static::class
		$class = get_called_class();

		if (!array_key_exists($class, self::$instances) || null == self::$instances[$class])
		{
			self::$instances[$class] = new $class();
		}

		return self::$instances[$class];
	}
}


