<?php

namespace Itgro\Helpers;

class MathHelpers
{
	public static function between($num, $min, $max = null)
	{
		if (null == $max)
		{
			if (is_array($min))
			{
				$max = max($min);
				$min = min($min);
			}
			else
			{
				throw new \InvalidArgumentException();
			}
		}

		return $num >= $min && $num <= $max;
	}
}
