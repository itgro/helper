<?php

namespace Itgro\Helpers;

use Itgro\IO;

class StringHelpers
{
	/**
	 * @param string $format Необходимый формат даты
	 * @param integer|null $sec Метка времени (null = текущее время)
	 * @param integer $round Округление
	 * @return string
	 */
	public static function timeFormat($format, $sec = null, $round = 0)
	{
		if (null === $sec)
		{
			$sec = time();
		}

		if ($round > 0)
		{
			$sec = round($sec / $round) * $round;
		}

		$pieces = [];

		$pieces[1]['h'] = floor($sec / 60 / 60);                // часы без ведущего ноля
		$pieces[1]['m'] = $sec / 60 % 60;                        // минуты без ведущего ноля
		$pieces[1]['s'] = $sec % 60;                            // секунды без ведущего ноля

		$pieces[1]['H'] = sprintf('%02d', $pieces[1]['h']);        // часы c ведущим нолем
		$pieces[1]['M'] = sprintf('%02d', $pieces[1]['m']);        // минуты c ведущим нолем
		$pieces[1]['S'] = sprintf('%02d', $pieces[1]['s']);        // секунды c ведущим нолем

		$pieces[2]['hh'] = sprintf('%.2f', $sec / 60 / 60);    // часы без ведущего ноля
		$pieces[2]['mm'] = sprintf('%.2f', $sec / 60);            // минуты без ведущего ноля
		$pieces[2]['ss'] = sprintf('%.2f', $sec);                // секунды без ведущего ноля

		$pieces[2]['HH'] = sprintf('%05s', $pieces[2]['hh']);    // часы c ведущим нолем
		$pieces[2]['MM'] = sprintf('%05s', $pieces[2]['mm']);    // минуты c ведущим нолем
		$pieces[2]['SS'] = sprintf('%02s', $pieces[2]['ss']);    // секунды c ведущим нолем

		foreach ([2, 1] as $range)
		{
			$format = str_replace(array_keys($pieces[$range]), $pieces[$range], $format);
		}

		return $format;
	}

	/**
	 * @param mixed $num Число, на основании которого формируется окончание
	 * @param string $e1 Склонение для числа 1 (ex: штука)
	 * @param string $e3 Склонение для числа 3 (ex: штуки)
	 * @param string $e5 Склонение для числа 5 (ex: штук)
	 *
	 * @return string
	 */
	public static function wordEnding($num, $e1, $e3, $e5)
	{
		$num %= 100;

		if ($num >= 11 && $num <= 19)
		{
			return $e5;
		}

		$num %= 10;

		if ($num == 1)
		{
			return $e1;
		}

		if ($num > 0 && $num <= 4)
		{
			return $e3;
		}

		return $e5;
	}

	/**
	 * @param $val
	 * @return bool
	 */
	public static function yes($val)
	{
		return in_array(static::lower($val), ['yes', 'y', 'да']) || $val === true;
	}

	/**
	 * @param $val
	 * @return bool
	 */
	public static function no($val)
	{
		return in_array(static::lower($val), ['no', 'n', 'нет']) || $val === false;
	}

	/**
	 * @param $string
	 * @return mixed|string
	 */
	public static function camel($string)
	{
		$string = ucwords(str_replace(['-', '_', '.'], ' ', $string));
		$string = str_replace(' ', '', $string);
		$string[0] = static::lower($string[0]);

		return $string;
	}

	/**
	 * @param $string
	 * @return string
	 */
	public static function snake($string)
	{
		$string = str_replace(['-', '.', ' '], '_', $string);
		$string = preg_replace('/(.)([A-ZА-Я])/u', '$1_$2', $string);
		$string = static::lower($string);
		while ($string !== ($string = str_replace('__', '_', $string))) ;
		return $string;
	}

	/**
	 * @param $string
	 * @return string
	 */
	public static function lower($string)
	{
		if (function_exists('mb_strtolower'))
		{
			return mb_strtolower($string);
		}

		return strtolower($string);
	}

	/**
	 * @param $string
	 * @return string
	 */
	public static function upper($string)
	{
		if (function_exists('mb_strtoupper'))
		{
			return mb_strtoupper($string);
		}

		return strtoupper($string);
	}

	/**
	 * @param $string
	 * @return string
	 */
	public static function capitalize($string)
	{
		return trim(static::lower($string));
	}

	/**
	 * @param mixed $var
	 * @param mixed $var,... [optional]
	 * @return string
	 */
	public static function serializeAll()
	{
		$result = '';

		foreach (func_get_args() as $value)
		{
			$result .= serialize($value);
		}

		return $result;
	}
}
