<?php

namespace Itgro\Helpers;

class ArrayHelpers
{
	/**
	 * @param array $array
	 * @param $regex
	 * @return array
	 */
	public static function keyMatch (array $array, $regex)
	{
		$only = array_filter(array_keys($array), function ($key) use ($regex)
		{
			return preg_match($regex, $key);
		});

		return array_only($array, $only);
	}

	/**
	 * @param array $array
	 * @param $keys
	 * @return array
	 */
	public static function only (array $array, $keys)
	{
		$result = [];

		$keys = (array) $keys;

		foreach ($keys as $key)
		{
			if (array_key_exists($key, $array))
			{
				$result[$key] = $array[$key];
			}
		}

		return $result;
	}

	/**
	 * @param array $array
	 * @param $keys
	 * @return array
	 */
	public static function exclude (array $array, $keys)
	{
		$result = $array;

		$keys = (array) $keys;

		foreach ($keys as $key)
		{
			if (array_key_exists($key, $result))
			{
				unset($result[$key]);
			}
		}

		return $result;
	}

	/**
	 * @param array $array
	 * @param $key
	 * @return array
	 */
	public static function pluck ($array, $key)
	{
		$results = [];

		foreach (explode('.', $key) as $segment)
		{
			$results = []; // Это не объявление, а сброс!

			foreach ((array) $array as $value)
			{
				if ($value instanceof \ArrayAccess)
				{
					$results[] = $value[$segment];
				}
				else if (is_object($value))
				{
					$results[] = $value->$segment;
				}
				else if (array_key_exists($segment, $value = (array) $value))
				{
					$results[] = $value[$segment];
				}
			}

			$array = array_values($results);
		}

		return array_values($results);
	}


	/**
	 * @param $array
	 * @param $key
	 * @return array
	 */
	public static function assoc ($array, $key)
	{
		$result = [];

		foreach ($array as $item)
		{
			if ($item instanceof \ArrayAccess)
			{
				$itemKey = $item[$key];
			}
			else if (is_object($item))
			{
				$itemKey = $item->$key;
			}
			else
			{
				$itemKey = $item[$key];
			}

			$result[$itemKey] = $item;
		}

		return $result;
	}

	/**
	 * @deprecated use array_filter
	 * @param array $array
	 * @param callable $callback
	 * @return array
	 */
	public static function find (array $array, \Closure $callback, $flag = 0)
	{
		return array_filter($array, $callback, $flag);
	}


	/**
	 * @param $array
	 * @return array
	 */
	public static function keysLower(array $array)
	{
		$result = [];

		foreach ($array as $key => $item)
		{
			if (is_array($item))
			{
				$item = static::keysLower($item);
			}

			$result[StringHelpers::lower($key)] = $item;
		}

		return $result;
	}

	/**
	 * @param array $array
	 * @return array
	 */
	public static function capitalize(array $array)
	{
		$result = [];

		foreach ($array as $key => &$item)
		{
			if (is_array($item))
				$item = static::capitalize($item);
			else if (is_scalar($item))
				$item = StringHelpers::capitalize($item);

			$result[StringHelpers::capitalize($key)] = $item;
		}

		return $result;
	}

}
