<?php
namespace Itgro;

use CHTTP;

/**
 * Class Http
 * @package Itgro
 */
class Http
{
	const
		#region Http statuses
			#region 1xx Information
			HTTP_CONTINUE							=	100,
			HTTP_SWITCHING_PROTOCOLS				=	101,
			HTTP_PROCESSING							=	102,
			HTTP_NAME_NOT_RESOLVED					=	105,
			#endregion
			#region 2xx Success
			HTTP_OK									=	200,
			HTTP_CREATED							=	201,
			HTTP_ACCEPTED							=	202,
			HTTP_NON_AUTHORITATIVE					=	203,
			HTTP_NO_CONTENT							=	204,
			HTTP_RESET_CONTENT						=	205,
			HTTP_PARTIAL_CONTENT					=	206,
			HTTP_MULTI_STATUS						=	207,
			HTTP_IM_USER							=	226,
			#endregion
			#region 3xx Redirection
			HTTP_MULTIPLE_CHOICES					=	300,
			HTTP_MOVED_PERMANENTLY					=	301,
			HTTP_MOVED_TEMPORARILY					=	302,
			HTTP_FOUND								=	302,
			HTTP_SEE_OTHER							=	303,
			HTTP_NOT_MODIFIED						=	304,
			HTTP_USE_PROXY 							=	305,
			#endregion
			#region 4xx Client error
			HTTP_BAD_REQUEST						=	400,
			HTTP_UNAUTHORIZED						=	401,
			HTTP_PAYMENT_REQUIRED					=	402,
			HTTP_FORBIDDEN							=	403,
			HTTP_NOT_FOUND							=	404,
			HTTP_METHOD_NOT_ALLOWED					=	405,
			HTTP_NOT_ACCEPTABLE						=	406,
			HTTP_PROXY_AUTHENTICATION_REQUIRED		=	407,
			HTTP_REQUEST_TIMEOUT					=	408,
			HTTP_CONFLICT							=	409,
			HTTP_GONE								=	410,
			HTTP_LENGTH_REQUIRED					=	411,
			HTTP_PRECONDITION_FAILED				=	412,
			HTTP_REQUEST_ENTITY_TOO_LARGE			=	413,
			HTTP_REQUEST_URI_TOO_LARGE				=	414,
			HTTP_UNSUPPORTED_MEDIA_TYPE				=	415,
			HTTP_REQUESTED_RANGE_NOT_SATISFIABLE	=	416,
			HTTP_EXPECTATION_FAILED					=	417,
			HTTP_I_AM_A_TEAPOT						=	418,
			HTTP_UNPOCESSABLE_ENTITY				=	422,
			HTTP_LOCKED								=	423,
			HTTP_FAILED_DEPENDENCY					=	424,
			HTTP_UNORDERED_COLLECTION				=	425,
			HTTP_UPGRADE_REQUIRED					=	426,
			HTTP_PRECONDITION_REQUIRED				=	428,
			HTTP_TOO_MANY_REQUESTS					=	429,
			HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE	=	431,
			HTTP_REQUEST_HOST_UNAVAILABLE			=	434,
			HTTP_RETRY_WITH							=	449,
			HTTP_UNAVAILABLE_FOR_LEGAL_REASONS		=	451,
			HTTP_UNRECOVERABLE_ERROR				=	456,
			#endregion
			#region 5xx Server error
			HTTP_INTERNAL_SERVER_ERROR				=	500,
			HTTP_NOT_IMPLEMENTED					=	501,
			HTTP_BAD_GATEWAY						=	502,
			HTTP_SERVICE_UNAVAILABLE				=	503,
			HTTP_GATEWAY_TIMEOUT					=	504,
			HTTP_VERSION_NOT_SUPPORTED				=	505,
			HTTP_VARIANT_ALSO_NEGOTIATES			=	506, /** @link https://tools.ietf.org/html/rfc2295#section-8.1 */
			HTTP_INSUFFICIENT_STORAGE				=	507,
			HTTP_LOOP_DETECTED						=	508,
			HTTP_BANDWIDTH_LIMIT_EXCEEDED			=	509,
			HTTP_NOT_EXTENDED						=	510,
			HTTP_NETWORK_AUTHENTICATION_REQUIRED	=	511;
		#endregion
	#endregion

	static $http_message = [
		#region 1xx Information
			Http::HTTP_CONTINUE								=>	'Continue',
			Http::HTTP_SWITCHING_PROTOCOLS					=>	'Switching Protocols',
			Http::HTTP_PROCESSING							=>	'Processing',
			Http::HTTP_NAME_NOT_RESOLVED					=>	'Name Not Resolved',
		#endregion
		#region 2xx Success
			Http::HTTP_OK									=>	'OK',
			Http::HTTP_CREATED								=>	'Created',
			Http::HTTP_ACCEPTED								=>	'Accepted',
			Http::HTTP_NON_AUTHORITATIVE					=>	'Non-Authoritative Information',
			Http::HTTP_NO_CONTENT							=>	'No Content ',
			Http::HTTP_RESET_CONTENT						=>	'Reset Content ',
			Http::HTTP_PARTIAL_CONTENT						=>	'Partial Content',
			Http::HTTP_MULTI_STATUS							=>	'Multi-Status',
			Http::HTTP_IM_USER								=>	'IM Used',
		#endregion
		#region 3xx Redirection
			Http::HTTP_MULTIPLE_CHOICES						=>	'Multiple Choices',
			Http::HTTP_MOVED_PERMANENTLY					=>	'Moved Permanently',
			Http::HTTP_MOVED_TEMPORARILY					=>	'Moved Temporarily',
			//Http::HTTP_FOUND								=>	'Found',
			Http::HTTP_SEE_OTHER							=>	'See Other',
			Http::HTTP_NOT_MODIFIED							=>	'Not Modified',
			Http::HTTP_USE_PROXY 							=>	'Use Proxy',
		#endregion
		#region 4xx Client error
			Http::HTTP_BAD_REQUEST							=>	'Bad Request',
			Http::HTTP_UNAUTHORIZED							=>	'Unauthorized',
			Http::HTTP_PAYMENT_REQUIRED						=>	'Payment Required ',
			Http::HTTP_FORBIDDEN							=>	'Forbidden',
			Http::HTTP_NOT_FOUND							=>	'Not Found',
			Http::HTTP_METHOD_NOT_ALLOWED					=>	'Method Not Allowed',
			Http::HTTP_NOT_ACCEPTABLE						=>	'Not Acceptable',
			Http::HTTP_PROXY_AUTHENTICATION_REQUIRED		=>	'Proxy Authentication Required',
			Http::HTTP_REQUEST_TIMEOUT						=>	'Request Timeout',
			Http::HTTP_CONFLICT								=>	'Conflict',
			Http::HTTP_GONE									=>	'Gone',
			Http::HTTP_LENGTH_REQUIRED						=>	'Length Required',
			Http::HTTP_PRECONDITION_FAILED					=>	'Precondition Failed',
			Http::HTTP_REQUEST_ENTITY_TOO_LARGE				=>	'Request Entity Too Large',
			Http::HTTP_REQUEST_URI_TOO_LARGE				=>	'Request-URL Too Long',
			Http::HTTP_UNSUPPORTED_MEDIA_TYPE				=>	'Unsupported Media Type',
			Http::HTTP_REQUESTED_RANGE_NOT_SATISFIABLE		=>	'Requested Range Not Satisfiable',
			Http::HTTP_EXPECTATION_FAILED					=>	'Expectation Failed',
			Http::HTTP_I_AM_A_TEAPOT						=>	'I\'m a teapot',
			Http::HTTP_UNPOCESSABLE_ENTITY					=>	'Unprocessable Entity',
			Http::HTTP_LOCKED								=>	'Locked',
			Http::HTTP_FAILED_DEPENDENCY					=>	'Failed Dependency',
			Http::HTTP_UNORDERED_COLLECTION					=>	'Unordered Collection',
			Http::HTTP_UPGRADE_REQUIRED						=>	'Upgrade Required',
			Http::HTTP_PRECONDITION_REQUIRED				=>	'Precondition Required',
			Http::HTTP_TOO_MANY_REQUESTS					=>	'Too Many Requests',
			Http::HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE		=>	'Request Header Fields Too Large',
			Http::HTTP_REQUEST_HOST_UNAVAILABLE				=>	'Requested Host Unavailable',
			Http::HTTP_RETRY_WITH							=>	'Retry With',
			Http::HTTP_UNAVAILABLE_FOR_LEGAL_REASONS		=>	'Unavailable For Legal Reasons',
			Http::HTTP_UNRECOVERABLE_ERROR					=>	'Unrecoverable Error',
		#endregion
		#region 5xx Server error
			Http::HTTP_INTERNAL_SERVER_ERROR				=>	'Internal Server Error',
			Http::HTTP_NOT_IMPLEMENTED						=>	'Not Implemented',
			Http::HTTP_BAD_GATEWAY							=>	'Bad Gateway',
			Http::HTTP_SERVICE_UNAVAILABLE					=>	'Service Unavailable',
			Http::HTTP_GATEWAY_TIMEOUT						=>	'Gateway Timeout',
			Http::HTTP_VERSION_NOT_SUPPORTED				=>	'HTTP Version Not Supported',
			Http::HTTP_VARIANT_ALSO_NEGOTIATES				=>	'Variant Also Negotiates',
			Http::HTTP_INSUFFICIENT_STORAGE					=>	'Insufficient Storage',
			Http::HTTP_LOOP_DETECTED						=>	'Loop Detected',
			Http::HTTP_BANDWIDTH_LIMIT_EXCEEDED				=>	'Bandwidth Limit Exceeded',
			Http::HTTP_NOT_EXTENDED							=>	'Not Extended',
			Http::HTTP_NETWORK_AUTHENTICATION_REQUIRED		=>	'Network Authentication Required',
		#endregion
	];

	/**
	 * @param $code
	 */
	public static function status ($status)
	{
		$code = intval($status);

		if ($code == $status && array_key_exists($code, static::$http_message))
		{
			$status .= " " . static::$http_message[$code];
		}

		CHTTP::SetStatus($status);

		if ($code == Http::HTTP_NOT_FOUND && !defined("ERROR_404"))
		{
			define("ERROR_404", "Y");
		}
	}

	/**
	 * @param $value
	 * @param int $status
	 * @param boolean $capitalize
	 */
	public static function send_json ($data, $status = Http::HTTP_OK, $capitalize = true)
	{
		static::clearOutput();

		header('Content-Type: application/json; charset=' . App::charset());

		static::status($status);

		if ($capitalize && is_array($data))
		{
			$data = array_keys_lower($data);
		}

		$params = (App::charset() == 'utf-8') ? JSON_UNESCAPED_UNICODE : 0;

		die(json_encode($data, $params));
	}

	public static function scriptRequested ()
	{
		$query = App::getInstance()->GetCurPage();
		$query = parse_url($query);

		$extension = pathinfo($query['path'], PATHINFO_EXTENSION);

		if ($extension)
		{
			if (preg_match('/ph(p[3456]?|t|tml)/', $extension))
			{
				return true;
			}

			return false;
		}

		return true;
	}

	/**
	 *
	 */
	public static function clearOutput ()
	{
		App::getInstance()->RestartBuffer();
	}

	public static function redirect ($url)
	{
		LocalRedirect($url, true, Http::HTTP_FOUND);
	}
}
