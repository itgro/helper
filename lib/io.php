<?

namespace Itgro;

use Bitrix\Main\IO\FileNotFoundException;
use Bitrix\Main\IO\IoException;

IncludeModuleLangFile(__FILE__);

class IO
{
    const CSV_DELIMITER = ",";

    /**
     * Записывает содержимое в файл.
     * Если файл не существует - он будет создан
     * Если директория не существует - она будет создана
     *
     * @param string    $fileName       полный путь до файла
     * @param string    $content        содержимое файла
     * @param bool      $append         дописывать ли в конец файла
	 * @param bool      $encode			кодировка получаемого файла
     * @param bool      $includePath    использовать ли include path
     * @param bool      $lock           нужно ли блокировать файл на время записи
     * @throws          IoException     в случае, если не удастся записать файл по любой причине
     */
    public static function writeFile ($fileName, $content, $append = false, $encode = null, $includePath = false, $lock = false)
    {
        $fileName = self::path($fileName);

        if(is_array($content))
		{
			$content = implode(PHP_EOL, $content);
		}

        self::makeDir(self::dirName($fileName));

        if(file_exists($fileName) && !is_writable($fileName))
		{
			throw new IoException("Файл $fileName недоступен для записи");
		}

        $mask = FILE_APPEND * (int)$append | FILE_USE_INCLUDE_PATH * (int)$includePath | LOCK_EX * (int)$lock;

		if (null !== $encode)
		{
			$detected = mb_detect_encoding($content, mb_detect_order());

			if (strcasecmp($detected, $encode) !== 0)
			{
				$content = mb_convert_encoding($content, $encode, $detected);
			}
		}

        if(@file_put_contents($fileName, $content, $mask) === false)
		{
			throw new IoException("Неудалось записать в файл $fileName");
		}
    }

    /**
     * Записывает данные в файл в формате csv
     * Если файл не существует - он будет создан
     * Если директория не существует - она будет создана
     *
     * @param string    $fileName       полный путь до файла
     * @param array[]   $rows           двумерный массив с данными
	 * @param string    $delimiter		нестандартный разделитель CSV
	 * @param string    $eol			символ конца строки
     * @param bool      $append         дописывать ли в конец файла
     * @param bool      $encode         кодировка получаемого файла
     * @param bool      $includePath    использовать ли include path
     * @param bool      $lock           нужно ли блокировать файл на время записи
     * @throws          IoException     в случае, если не удастся записать файл по любой причине
     */
    public static function writeCsv($fileName, $rows, $delimiter = null, $eol = null, $append = false, $encode = null, $includePath = false, $lock = false)
    {
		if (null == $delimiter)
		{
			$delimiter = self::CSV_DELIMITER;
		}

		if (null == $eol)
		{
			$eol = PHP_EOL;
		}

        foreach ($rows as &$row)
		{
			foreach ($row as &$r)
			{
				if (stripos($r, $delimiter) !== false || stripos($r, $eol) !== false)
				{
					$r = '"' . str_replace('"', '""', $r) . '"';
				}
			}

			$row = implode($delimiter, $row);
		}

        $rows = implode($eol, $rows);

		if ($append == true)
		{
			$rows = $eol . $rows;
		}

        self::writeFile($fileName, $rows, $append, $encode, $includePath, $lock);
    }

    /**
     * Читает файл в строку и возвращает содержимое
     *
     * @param string    $fileName               полный путь до файла
     * @throws          FileNotFoundException   в случае, если файл не существует
     * @throws          IoException             в случае, если файл недоступен для чтения
     *                                          или файл не удалось прочитать
     *
     * @return string
     */
    public static function readFile ($fileName)
    {
        $fileName = self::path($fileName);

		if(!file_exists($fileName))
		{
			throw new FileNotFoundException("Файл $fileName не существует");
		}

		if(!is_readable($fileName))
		{
			throw new IoException("Файл $fileName недоступен для чтения");
		}

        $extension = self::extension($fileName);

        if('gz' == $extension)
		{
			$content = implode('',gzfile($fileName));
		}
        else if('bz2' == $extension && function_exists("bzfile"))
		{
			$content = bzfile($fileName);
		}
        else if(!$content = @file_get_contents($fileName))
		{
			throw new IoException("Файл $fileName недоступен для чтения");
		}

        if ($content === false)
		{
			throw new IoException("Не удалось прочитать файл $fileName");
		}

        return $content;
    }

    /**
     * Читает файл с данными в формате csv и возвращает в виде двухмерного массива
     *
     * @param string    $fileName               полный путь до файла
     * @param string    $delimiter				нестандартный разделитель CSV
     * @param string    $eol					символ конца строки
     * @throws          FileNotFoundException   в случае, если файл не существует
     * @throws          IoException             в случае, если файл недоступен для чтения
     *                                          или файл не удалось прочитать
     *
     * @return array[]
     */
    public static function readCsv ($fileName, $delimiter = null)
    {
		if (null == $delimiter)
		{
			$delimiter = static::CSV_DELIMITER;
		}

		if(!file_exists($fileName))
		{
			throw new FileNotFoundException("Файл $fileName не существует");
		}

		if(!is_readable($fileName))
		{
			throw new IoException("Файл $fileName недоступен для чтения");
		}

		$handle = fopen(IO::path($fileName), 'r+');

		if (!$handle)
		{
			throw new IoException("Не удалось прочитать файл $fileName");
		}

		$content = [];

		while ($row = fgetcsv($handle, null, $delimiter))
		{
			$content[] = $row;
		}

		return $content;
    }

    /**
     * Читает содержимое каталога и возвращает в виде массива
     * Из результата исключаются ссылки на текущий и родительский каталог ("." и "..")
     * Из результата исключаются файлы и каталоги, недоступные для чтения
     *
     * @param string    $dirName        полный путь до каталога
     *
     * @throws          IoException     если каталог не существует, находится вне
     *                                  директории сайта или недоступен для чтения
     * @return array
     */
    public static function readDir ($dirName)
    {
        $dirName = self::path($dirName);

        if(!is_dir($dirName) || !is_readable($dirName))
		{
			return array();
		}

        if(stripos($dirName, self::root()) === false)
		{
			throw new IoException("Каталог $dirName должен находится внутри директории сайта");
		}

        $files = scandir($dirName);
        $result = array();

        foreach ($files as $file)
        {
            if($file == "." || $file == "..")
			{
				continue;
			}

            $fullFileName = self::path($dirName . "/" . $file);

            if(file_exists($fullFileName) && is_readable($fullFileName))
			{
				$result[] = $file;
			}
        }

        return $result;
    }

    /**
     * @param string    $dirName        полный путь до каталога
     * @param int|null  $mode           права на доступ к файлу
     *                                  (по умолчанию равен BX_DIR_PERMISSIONS)
     *
     * @throws          IoException     если каталог аходится вне директории сайта,
     *                                  если каталог с таким именем уже существует,
     *                                  если родительский каталог недоступен для записи
     */
    public static function makeDir ($dirName, $mode = null)
    {
        $dirName = self::path($dirName);

        if (!is_int($mode))
		{
			$mode = BX_DIR_PERMISSIONS;
		}

        if(stripos(self::path($dirName."/"), self::root()) === false)
		{
			throw new IoException("Каталог $dirName должен находится внутри директории сайта");
		}

        if (file_exists($dirName))
        {
            if (!is_dir($dirName))
            {
                throw new IoException("Невозможно создать каталог {$dirName}. Файл с таким именем уже существует");
            }
            else if (is_writable($dirName))
            {
                chmod($dirName, $mode);
                return;
            }
        }

        if (!@mkdir($dirName, $mode, true))
		{
			throw new IoException("Каталог $dirName недоступен для записи");
		}
    }

    /**
     * Копирует файл
     *
     * @param string    $origFileName           полный путь до файла
     * @param string    $targetFileName         имя нового (целиком, полный путь)
     * @throws          FileNotFoundException   в случае, если файл не существует
     * @throws          IoException             в случае, если файл недоступен для чтения,
     *                                          новый файл недоступен для записи,
     *                                          один из путей находится вне директории сайта
     *                                          или не может быть прочитан или записан по любой другой причине
     */
    public static function copyFile ($origFileName, $targetFileName)
    {
        $origFileName = self::path($origFileName);
        $targetFileName = self::path($targetFileName);

        $content = self::readFile($origFileName);
        self::writeFile($targetFileName, $content);
    }

	/**
	 * Перемещаяет файл
	 *
	 * @param string	$origFileName			полный путь до файла
	 * @param string 	$targetFileName			месторасположение и имя нового файла
	 * @throws 			FileNotFoundException	в случае, если файл не существует
	 * @throws          IoException             в случае, если файл недоступен для чтения,
	 *                                          новый файл недоступен для записи,
	 *                                          один из путей находится вне директории сайта
	 *                                          или не может быть прочитан или записан по любой другой причине
	 */
    public static function moveFile ($origFileName, $targetFileName)
    {
        $origFileName = self::path($origFileName);
        $targetFileName = self::path($targetFileName);

		if(!file_exists($origFileName))
		{
			throw new FileNotFoundException("Файл $origFileName не существует");
		}

        if(!is_readable($origFileName))
		{
			throw new IoException("Файл $origFileName недоступен для чтения");
		}

        if(file_exists($targetFileName) && !is_writable($targetFileName))
		{
			throw new IoException("Файл $targetFileName недоступен для записи");
		}

        rename($origFileName, $targetFileName);
    }

	/**
	 * Удаляет файл
	 *
	 * @param string 	$fileName	имя файла
	 * @throws          IoException в случае, если файл недоступен для чтения,
	 *                              новый файл недоступен для записи,
	 *                              один из путей находится вне директории сайта
	 *                              или не может быть удален по любой другой причине
	 */
    public static function deleteFile ($fileName)
    {
        $fileName = self::path($fileName);

        if(file_exists($fileName) && !is_writable($fileName))
		{
			throw new IoException("Файл $fileName недоступен для записи");
		}

        if(file_exists($fileName))
		{
			unlink($fileName);
		}
    }

	/**
	 * Исправляет слеши в имени файла или директории
	 *
	 * @param string 	$path имя файла или директории
	 * @return string	исправленный $path
	 */
    public static function path ($path)
    {
        while($path != ($path = str_replace(array("//", "\\"), "/", $path)));

		if (file_exists($path))
		{
			$path = realpath($path);
		}

        return $path;
    }

	/**
	 * Удаляет из имени файла или папки путь до сайта
	 *
	 * @param string 	$path имя файла или директории
	 * @return string	исправленный $path
	 */
	public static function relative ($path)
	{
		return str_replace(static::root() . '/', '', static::path($path));
	}

	/**
	 * Определяет разрешение файла
	 *
	 * @param string 	$path имя файла
	 * @return string	разрешение без точки
	 */
    public static function extension($path)
    {
        $path = self::path($path);
        return pathinfo($path, PATHINFO_EXTENSION);
    }

	/**
	 * Получает имя файла из полного пути
	 *
	 * @param string 	$path полный путь до файла
	 * @return string	имя файла
	 */
    public static function fileName($path)
    {
        $path = self::path($path);
        return pathinfo($path, PATHINFO_FILENAME);
    }

	/**
	 * Получает имя директории из полного пути
	 *
	 * @param string 	$path путь
	 * @return string	имя директории
	 */
    public static function dirName($path)
    {
        $path = self::path($path);
        return pathinfo($path, PATHINFO_DIRNAME);
    }

	/**
	 * Определяет тип файла
	 *
	 * @param string 	$path имя файла
	 * @return string	тип файла
	 */
    public static function mimeType($path)
    {
        $path = self::path($path);
        $mime = finfo_open(FILEINFO_MIME);
        return finfo_file($mime, $path);
    }

	/**
	 * Получает корневую директорию сайта
	 *
	 * @return string	путь до корневой директории сайта
	 */
    public static function root()
    {
        if (array_key_exists("DOCUMENT_ROOT", $_SERVER))
		{
			return self::path($_SERVER["DOCUMENT_ROOT"]);
		}
        else
		{
			return self::path(\Bitrix\Main\Application::getDocumentRoot() . "/");
		}
    }

	/**
	 * Записывает информацию в лог
	 * Файл будет размещен в корневой директории сайта
	 * В папке logs, в файле {$logName}.log
	 *
	 * @param string	$logName имя лога
	 * @param mixed		$data
	 * @throws 			IoException
	 */
    public static function writeLog($logName, $data)
    {
        if(!is_array($data))
		{
			$data = array($data);
		}

        $content = str_repeat("=", 10) . date("d.m.Y H:i:s") . str_repeat("=", 10) . str_repeat(PHP_EOL, 2);

        $content .= Dumper::dumpAsString($data, false);

        $content .= str_repeat(PHP_EOL, 2);

        self::writeFile(self::root() . "/logs/" . strval($logName) . ".log", $content, true);
    }
}
