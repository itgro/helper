<?php
namespace Itgro;

/**
 * Class Debug
 * @package Itgro
 */
class Debug
{
	static $start = [];
	static $count = 0;

	static $colors = [
		'#38AD40',
		'#ff9300',
		'#ff0027',
	];

	public static function start ()
	{
		static::$start[] = microtime(true);
	}

	public static function stop ()
	{
		return array_pop(static::$start);
	}

	public static function reset ()
	{
		static::stop();
		static::start();
	}

	public static function save ($name = null)
	{
		if ($name == null)
		{
			$trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
			$name = IO::relative(reset($trace)['file']);
		}

		$start = static::stop();
		$diff = microtime(true) - $start;

		$sec = floor($diff);

		$msec = round($diff - $sec, 3);

		static::show($name, $sec, $msec);
	}

	private static function show ($name, $sec, $msec)
	{
		$color = static::$colors[0];

		if ($sec > 10)
		{
			$color = static::$colors[2];
		}
		elseif ($sec > 5)
		{
			$color = static::$colors[1];
		}

		if ($sec >= 60 * 60)
		{
			$sec = time_format('H:M:S', $sec);
		}
		else if ($sec >= 60)
		{
			$sec = time_format('M:S', $sec);
		}

		if ($msec > 0)
		{
			$sec = $sec . '.' . end(explode('.', rtrim($msec, '0')));
		}

		$style = join('; ', [
			'z-index: 1000',
			//'position: fixed',
			'box-sizing: border-box',
			'display: block',
			'white-space: nowrap',
			'padding: 5px',
			'border: 1px solid #000',
			'color: #FFF',
			'background: ' . $color,
		]);

		$element = '<span style="'.$style.'">' . $name . ': <span style="font-size: 150%;">' . $sec . '</span> s.</span>';

		echo $element;
	}
}


