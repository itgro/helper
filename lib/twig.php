<?php

namespace Itgro;

if (!class_exists('Twig_Loader_Filesystem'))
{
	throw new \Exception ('twig не установлен');
}

class Twig
{
	/** @var \Twig_Environment $instance  */
	private static $instance = null;

	/**
	 * @return \Twig_Environment
	 */
	public static function getInstance()
	{
		if (static::$instance == null)
		{
			static::init();
		}

		return static::$instance;
	}

	public static function init ()
	{
		$loader = new \Twig_Loader_Filesystem($_SERVER['DOCUMENT_ROOT'] . '/local/twig/');

		static::$instance = new \Twig_Environment($loader, array(
			'cache' => false,//$_SERVER['DOCUMENT_ROOT'] . '/bitrix/cache_twig/',
		));
	}
}
