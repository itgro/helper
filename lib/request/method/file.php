<?
namespace Itgro\Request\Method;

use Bitrix\Main\ArgumentException;

if (!class_exists('Itgro\Request\Method\File'))
{
	class File extends Base
	{
		public function all ()
		{
			$result = [];

			foreach ((array) $_FILES as $key => $file)
			{
				if (UploadedFile::is($file))
				{
					$result[$key] = new UploadedFile($file);
				}
				else
				{
					$result[$key] = UploadedFile::getArray($file);
				}
			}

			return $result;
		}
	}
}

if (!class_exists('Itgro\Request\Method\UploadedFile'))
{
	class UploadedFile
	{
		private $name,
			$tmp_name,
			$type,
			$size,
			$error;

		public function __construct ($file)
		{
			if (is_uploaded_file($file['tmp_name']))
			{
				$this->name = $file['name'];
				$this->tmp_name = $file['tmp_name'];
				$this->type = $file['type'];
				$this->size = $file['size'];
				$this->error = $file['error'];
			}
			else
			{
				throw new ArgumentException();
			}
		}

		public function move($destination)
		{
			return move_uploaded_file($this->tmp_name, $destination);
		}

		public function name ()
		{
			return $this->name;
		}

		public function tmp_name ()
		{
			return $this->tmp_name;
		}

		public function type ()
		{
			return $this->type;
		}

		public function size ()
		{
			return $this->size;
		}

		public function check ()
		{
			return $this->error == UPLOAD_ERR_OK;
		}

		public static function is ($file)
		{
			return array_key_exists('tmp_name', $file) && is_uploaded_file($file['tmp_name']);
		}

		public static function getArray($array)
		{
			$result = [];

			foreach ($array as $key => $item)
			{
				if (static::is($item))
				{
					$result[$key] = new static($item);
				}
				else if (is_array($item))
				{
					$result[$key] = static::getArray($item);
				}
			}

			return $result;
		}
	}
}
