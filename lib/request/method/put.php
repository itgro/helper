<?
namespace Itgro\Request\Method;

class Put extends Base
{
	private $data = [];

	public function __construct ()
	{
		$data = null;

		if (function_exists('file_get_contents'))
		{
			$data = file_get_contents('php://input');
		}
		else if (isset($HTTP_RAW_POST_DATA))
		{
			$data = $HTTP_RAW_POST_DATA;
		}

		$this->data = json_decode($data, true);
	}

	public function all ()
	{
		return $this->escape($this->data);
	}
}
