<?
namespace Itgro\Request\Method;

use Bitrix\Main\Application;

abstract class Base
{
	abstract public function all ();

	public function only (array $keys)
	{
		return array_only($this->all(), $keys);
	}

	public function exclude (array $keys)
	{
		return array_exclude($this->all(), $keys);
	}

	public function get ($key)
	{
		$current = $this->all();

		foreach ($this->explodeParamArray($key) as $item)
		{
			$current = $current[$item];
		}

		if ($current == $this->all())
		{
			$current = null;
		}

		return $current;
	}

    public function escape ($value)
    {
        $conn = Application::getInstance()->getConnection()->getSqlHelper();

        if (is_scalar($value))
        {
            return $conn->forSql($value);
        }
        else
        {
            array_walk($value, [$this, 'escape']);
            return $value;
        }
    }

	public function has ($key, $value = null)
	{
		if (is_array($key))
		{
			foreach ($key as $k)
			{
				if (!static::has($k))
				{
					return false;
				}
			}

			return true;
		}

		return !is_null($this->get($key)) && ($value === null || $this->all()[$key] == $value);
	}

	public function explodeParamArray ($string)
	{
		$result = [];

		$buffer = '';

		$openBrackets = false;
		$firstBrackets = true;

		$string = (string) $string;

		$length = strlen($string);

		for ($i = 0; $i < $length; $i++)
		{
			$needFlush = false;

			$char = $string[$i];

			if ($char == '[' || $char == ']')
			{
				if ($char == '[')
				{
					if ($firstBrackets)
					{
						if (strlen($buffer) < 1)
						{
							break;
						}

						$needFlush = true;
						$firstBrackets = false;
					}

					if (!$openBrackets)
						$openBrackets = true;
				}
				elseif ($char == ']')
				{
					if ($openBrackets)
						$openBrackets = false;

					$needFlush = true;
				}
			}
			else
			{
				if ($openBrackets || $firstBrackets)
				{
					if ($firstBrackets && $i == $length - 1)
					{
						$needFlush = true;
					}

					$buffer .= $char;
				}
				else
				{
					break;
				}
			}

			if ($needFlush)
			{
				if (strlen($buffer) > 0)
				{
					$result[] = $buffer;
				}
				else if ($char == ']')
				{
					$result[] = 0;
				}

				$buffer = '';
			}
		}

		return $result;
	}
}
