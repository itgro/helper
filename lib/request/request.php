<?
namespace Itgro\Request;

use Bitrix\Main\Application;
use Itgro\Request\Method\Base as MethodInterface;

class Request
{
	private static $methods = [];

	/**
	 * @param $method
	 * @return MethodInterface
	 */
	public static function getMethod ($method)
	{
		if (!array_key_exists($method, static::$methods) || is_null(static::$methods[$method]))
		{
			$className = static::className($method);

			if (class_exists($className) && is_subclass_of($className, 'Itgro\Request\Method\Base'))
			{
				static::$methods[$method] = new $className();
			}
			else
			{
				return false;
			}
		}

		return static::$methods[$method];
	}

	/**
	 * @param string $class
	 * @return string
	 */
	private static function className($class)
	{
		return 'Itgro\\Request\\Method\\' . ucfirst(ToLower($class));
	}

	/**
	 * @return MethodInterface
	 */
	public static function post()
	{
		return static::getMethod('Post');
	}

	/**
	 * @return MethodInterface
	 */
	public static function get()
	{
		return static::getMethod('Get');
	}

	/**
	 * @return MethodInterface
	 */
	public static function request()
	{
		return static::getMethod('Request');
	}

	/**
	 * @return MethodInterface
	 */
	public static function put()
	{
		return static::getMethod('Put');
	}


	/**
	 * @return MethodInterface
	 */
	public static function file()
	{
		return static::getMethod('File');
	}

	/**
	 * @return bool
	 */
	public static function is_post()
	{
		return static::method() == 'POST';
	}

	/**
	 * @return bool
	 */
	public static function is_get()
	{
		return static::method() == 'GET';
	}

	/**
	 * @return bool
	 */
	public static function is_put()
	{
		return static::method() == 'PUT';
	}

	/**
	 * @return null|string
	 */
	public static function method()
	{
		static $method = null;

		if ($method != null)
		{
			return $method;
		}

		$context = Application::getInstance()->getContext();

		if ($context != null)
		{
			$server = $context->getServer();

			if ($server != null)
			{
				return $method = $server->getRequestMethod();
			}
		}

		return 'GET';
	}
}
