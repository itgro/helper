<?php
namespace Itgro;


use Itgro\Request\Request;

class Form
{
	static $started = false;
	static $method = null;

	static $valuesCache = [];

    public static function start ($action, $method = 'post', array $attributes = [])
	{
		$attributes['action'] = $action;
		$attributes['method'] = $method;
		static::$started = true;
		static::$method = $method;

		echo '<form';

		static::attributes($attributes);

		echo '>';
	}

	public static function end ()
	{
		static::$started = false;
		static::$method = null;

		echo '</form>';
	}

	public static function input ($type, $name, $value = null, $default = null, array $attributes = [])
	{
		echo '<input';

		$attributes['type'] = $type;
		$attributes['name'] = $name;

		if (null !== $value)
		{
			$attributes['value'] = $value;
		}
		else
		{
			if (null !== $default)
			{
				$attributes['value'] = $default;
			}

			if (static::$started)
			{
				if ($method = Request::getMethod(static::$method))
				{
					if ($method->has($name))
					{
						if (!array_key_exists($name, static::$valuesCache))
						{
							static::$valuesCache[$name] = (array) $method->get($name);
						}

						if (count(static::$valuesCache[$name]) > 0)
						{
							$attributes['value'] = array_shift(static::$valuesCache[$name]);
						}
					}
				}
			}
		}

		static::attributes($attributes);

		echo '/>';
	}

	public static function checkbox ($name, $value = null, $checked = null, array $attributes = [])
	{
		if (null == $value)
		{
			$value = 1;
		}

		if (array_key_exists('checked', $attributes))
		{
			unset ($attributes['checked']);
		}

		// TODO detect checked from request

		if ($checked)
		{
			$attributes['checked'] = 'checked';
		}

		static::input('checkbox', $name, $value, null, $attributes);
	}

	public static function select ($name, array $values, $value = null, $default = null, $multiple = false, array $attributes = [])
	{
		echo '<select';

		$attributes['name'] = $name;

		if ($multiple)
		{
			$attributes['multiple'] = 'multiple';
		}

		static::attributes($attributes);

		echo '>';

		if (null === $value)
		{
			if (null !== $default)
			{
				$value = $default;
			}

			if (static::$started)
			{
				if ($method = Request::getMethod(static::$method))
				{
					if ($method->has($name))
					{
						if (!array_key_exists($name, static::$valuesCache))
						{
							static::$valuesCache[$name] = (array) $method->get($name);
						}

						if (count(static::$valuesCache[$name]) > 0)
						{
							if ($multiple)
							{
								$value = static::$valuesCache[$name];
								static::$valuesCache[$name] = [];
							}
							else
							{
								$value = array_shift(static::$valuesCache[$name]);
							}
						}
					}
				}
			}
		}

		foreach ($values as $k => $v)
		{
			$option = [];
			$n = null;

			echo '<option';

			if (is_array($v))
			{
				if (array_key_exists('value', $v))
				{
					$option['value'] = $v['value'];

					if (array_key_exists('name', $v))
					{
						$n = $v['name'];
					}
				}
				else
				{
					$shift = $v;
					$option['value'] = array_shift($shift);

					if (!empty($shift))
					{
						$n = array_shift($shift);
					}

					unset($shift);
				}
			}
			else
			{
				$n = $v;
				$option['value'] = $k;
			}

			if (is_array($value) && in_array($option['value'], $value))
			{
				$option['selected'] = 'selected';
			}
			else if ($option['value'] == $value)
			{
				$option['selected'] = 'selected';
			}

			static::attributes($option);

			echo '>';

			if (is_null($n))
			{
				echo $option['value'];
			}
			else
			{
				echo $n;
			}

			echo '</option>';
		}

		echo '</select>';
	}

	private static function attributes (array $attributes = [])
	{
		if (null !== $attributes)
		{
			foreach ($attributes as $name => $value)
			{
				echo ' ' . Html::encode($name) . '="' . Html::encode($value) . '"';
			}
		}
	}
}


