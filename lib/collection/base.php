<?php

namespace Itgro\Collection;

/**
 * @package Itgro
 */
abstract class Base implements \IteratorAggregate, \ArrayAccess, \Serializable, \Countable
{
	/**
	 * Возвращает все элементы коллекции
	 * @return array
	 */
	abstract public function all ();

	/**
	 * Добавляет или заменяет элемент
	 *
	 * @param mixed $key ключ элемента
	 * @param mixed $value новое значение
	 * @return null
	 */
	abstract public function set ($key, $value);

	/**
	 * Добавляет новый элемент в конец коллекции
	 *
	 * @param mixed $value новый элемент
	 * @return null
	 */
	abstract public function push ($value);

	/**
	 * Удаляет элемент $key
	 *
	 * @param
	 * @return null
	 */
	abstract public function remove ($key);

	abstract public function fetch ();

	abstract public function reset ();

	abstract public function first ();

	public function only (array $keys)
	{
		return array_only($this->all(), $keys);
	}

	public function get ($key)
	{
		if ($this->has($key))
		{
			return $this->all()[$key];
		}

		return null;
	}

	public function has ($key)
	{
		if (is_array($key))
		{
			foreach ($key as $k)
			{
				if (!$this->has($k))
				{
					return false;
				}
			}

			return true;
		}

		return array_key_exists($key, $this->all());
	}

	public function pluck ($key)
	{
		return array_pluck($this->all(), $key);
	}

	abstract public function each (\Closure $callback);

	#region Magic

	public function __debugInfo ()
	{
		return $this->__toArray();
	}

	public function __toArray ()
	{
		return $this->all();
	}

	public function __get ($key)
	{
		if ($this->offsetExists($key))
		{
			return $this->all()[$key];
		}

		return null;
	}

	public function __set ($key, $value)
	{
		$this->set($key, $value);
	}


	public function __isset ($key)
	{
		return $this->offsetExists($key);
	}

	public function __unset ($key)
	{
		if ($this->offsetExists($key))
		{
			$this->remove($key);
		}
	}

	#endregion

	#region ArrayAccess

	public function offsetExists ($offset)
	{
		return array_key_exists($offset, $this->all());
	}


	public function offsetGet ($offset)
	{
		if ($this->offsetExists($offset))
		{
			return $this->all()[$offset];
		}

		return null;
	}

	public function offsetSet ($offset, $value)
	{
		$this->all()[$offset] = $value;
	}

	public function offsetUnset ($offset)
	{
		if ($this->offsetExists($offset))
		{
			$this->remove($offset);
		}
	}

	#endregion

	#region Countable

	public function count()
	{
		return count($this->all());
	}

	#endregion

	#region IteratorAggregate

	public function getIterator()
	{
		return new \ArrayIterator($this->all());
	}

	#endregion

	#region Serializable

	public function serialize()
	{
		return $this->serialize($this->all());
	}

	public function unserialize($serialized)
	{
		$this->data = unserialize($serialized);
	}

	#endregion
}


