<?php

namespace Itgro\Collection;

use Bitrix\Main\NotImplementedException;
use CAllDBResult;

/**
 * @package Itgro
 */
class DBCollection extends Base
{
	private $result = null,
			$data = [],
			$key = null;

	public function __construct (CAllDBResult $result, $key = null)
	{
		$this->result = $result;

		if ($key)
		{
			$this->key = $key;
		}

		return $this;
	}

	public function all()
	{
		while ($this->fetch());

		return $this->data;
	}

	public function set($key, $value)
	{
		$this->data[$key] = $value;
	}

	public function push($value)
	{
		$this->data[] = $value;
	}

	public function remove($key)
	{
		throw new NotImplementedException('Нельзя удалять результаты выборки');
	}

	public function fetch()
	{
		if ($data = $this->result->fetch())
		{
			if ($this->key)
			{
				$this->set($data[$this->key], $data);
			}
			else
			{
				$this->push($data);
			}

			return $data;
		}

		return false;
	}

	public function reset()
	{
		// TODO Сделать нормальный метод
		mysql_data_seek($this->result->result, 0);
	}

	public function first()
	{
		foreach ($this->all() as $data)
		{
			return $data;
		}

		return null;
	}

	public function each (\Closure $callback)
	{
		$this->all();
		return array_walk($this->data, $callback);
	}
}


