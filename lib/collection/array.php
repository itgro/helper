<?php

namespace Itgro\Collection;

/**
 * @package Itgro
 */
class ArrayCollection extends Base
{
	protected $data = [];

	public function __construct (array $data = null)
	{
		if (null !== $data)
		{
			$this->data = $data;
		}

		return $this;
	}

	public function all()
	{
		return $this->data;
	}

	public function set($key, $value)
	{
		$this->data[$key] = $value;
		return $this;
	}

	public function push($value)
	{
		$this->data[] = $value;
		return $this;
	}

	public function remove($key)
	{
		if ($this->has($key))
		{
			unset($this->data[$key]);
		}

		return $this;
	}

	public function fetch ()
	{
		$result = current($this->data);
		next($this->data);
		return $result;
	}

	public function reset ()
	{
		return reset($this->data);
	}

	public function first ()
	{
		foreach ($this->data as $row)
		{
			return $row;
		}

		return null;
	}

	public function each (\Closure $callback)
	{
		return array_walk($this->data, $callback);
	}

	public function merge ($target)
	{
		if (is_a($target, 'Itgro\Collection\Base'))
		{
			/** @var Base $target */
			$target = $target->all();
		}
		else if (!is_array($target))
		{
			$target = (array) $target;
		}

		/** @var array $target */

		$this->data = array_merge($this->data, $target);

		return $this;
	}
}


