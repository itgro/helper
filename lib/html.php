<?php
namespace Itgro;


class Html
{
    public static function a($label, $link, $params = [])
    {
        if (strlen($label) == 0 || null == $label)
            $label = $link;

        $result = "<a href=\"$link\"";

        foreach ($params as $name => $value)
            $result .= " $name=\"$value\"";

        $result .= ">$label</a>";

        return $result;
    }

    public static function decode ($html)
    {
        return htmlspecialchars_decode($html);
    }

    public static function encode ($html)
    {
        return htmlspecialchars($html);
    }
}


