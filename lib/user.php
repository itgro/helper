<?php
namespace Itgro;

use CUser;

/**
 * Class User
 * @package Itgro
 */
class User
{
	private static $user;

	/**
	 * @return CUser
	 */
	private static function findUser ()
	{
		if ($GLOBALS['USER'] instanceof CUser)
		{
			return $GLOBALS['USER'];
		}

		return null;
	}

	/**
	 * @return CUser
	 */
	public static function getInstance ()
	{
		if (is_null(static::$user))
		{
			static::$user = static::findUser();
		}

		return static::$user;
	}

	/**
	 * @static
	 * @param $method
	 * @param $args
	 * @throws \InvalidArgumentException
	 * @return mixed
	 */
	public function __call ($method, $args)
	{
		$user = $this->getInstance();

		if (method_exists($user, $method))
		{
			return call_user_func_array([$user, $method], $args);
		}

		throw new \InvalidArgumentException();
	}

	/**
	 * @static
	 * @param $method
	 * @param $args
	 * @throws \InvalidArgumentException
	 * @return mixed
	 */
	public static function __callStatic ($method, $args)
	{
		$user = new static();

		return call_user_func_array([$user, $method], $args);
	}
}


