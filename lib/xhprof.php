<?php
namespace Itgro;

use XHProfRuns_Default;

/**
 * Class Xhprof
 * @package Itgro
 */
class Xhprof
{
	private static $started = false;

	public function loaded ()
	{
		return extension_loaded('xhprof');
	}

	public function installed ()
	{
		return class_exists('XHProfRuns_Default');
	}

	public function start ()
	{
		$enabled = \COption::GetOptionInt('helper', 'xhprof_profiling');

		if ($enabled && static::loaded() && App::development())
		{
//			xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);
			static::$started = true;
		}
	}

	public function stop ()
	{
		if (static::$started && static::loaded() && static::installed() && App::development())
		{
			$xhprof_runs = new XHProfRuns_Default();

			$run_id = $xhprof_runs->save_run(xhprof_disable(), 'profile');

			if (!App::ajax())
			{
				echo "<div id='xhprof' style='position:fixed;bottom:0;left:0;padding:10px;background:#fff;border:1px solid red;' ondblclick='$(this).remove();'><a href='/vendor/lox/xhprof/xhprof_html/index.php?run=".$run_id."&source=profile&sort=excl_wt' target='_blank'>xhprof report</a></div>";
			}
		}
	}
}


