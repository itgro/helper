<?php
namespace Itgro\Composer\Installers;

use Composer\Package\PackageInterface;

class Installer extends LibraryInstaller
{
	/**
	 * {@inheritDoc}
	 */
	public function getInstallPath(PackageInterface $package)
	{
		return 'local/modules/{$name}/';
	}
}
