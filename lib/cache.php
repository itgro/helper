<?php
namespace Itgro;

use CCacheManager;
use CPHPCache;

/**
 * Class Cache
 * @package Itgro
 */
class Cache extends CPHPCache
{
	/** @var  CCacheManager */
	protected static $manager;

	private $cacheParams = [];

	public function __construct ($time, $cache_id, $dir = false, $basedir = "cache")
	{
		$this->cacheParams = func_get_args();
		return parent::__construct();
	}

	public function get (\Closure $callback)
	{
		if (call_user_func_array([$this, 'InitCache'], $this->cacheParams))
		{
			return $this->GetVars();
		}

		$result = $callback();

		if ($result !== false && $this->StartDataCache())
		{
			$this->EndDataCache($result);
		}

		return $result;
	}

	/**
	 *
	 * @return CCacheManager
	 */
	public static function manager ()
	{
		if (null == static::$manager)
		{
			static::$manager = $GLOBALS['CACHE_MANAGER'];
		}

		return static::$manager;
	}
}
