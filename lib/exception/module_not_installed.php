<?php
namespace Itgro\Exception;

use Exception;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class ModuleNotIncludedException
 * @package Itgro\Exception
 */
class ModuleNotIncludedException extends BaseException
{
	public function __construct ($moduleName = null, $code = 0, Exception $previous = null)
	{
		if (null == $moduleName)
		{
			$message = GetMessage('ITGRO_MODULE_NOT_INSTALLED');
		}
		else
		{
			$message = GetMessage('ITGRO_MODULE_BY_NAME_NOT_INSTALLED', ['#name#' => $moduleName]);
		}

		parent::__construct($message, $code, $previous);
	}
}
