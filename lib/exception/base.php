<?php
namespace Itgro\Exception;

use Exception;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class ModuleNotIncludedException
 * @package Itgro\Exception
 */
abstract class BaseException extends Exception
{

}
