<?php
namespace Itgro;

/**
 * Class Debug
 * @package Itgro
 */
class HtmlDumper extends \Symfony\Component\VarDumper\Dumper\HtmlDumper
{
    protected $styles = array(
        'default' => 'background-color:#transparent; color:#000; line-height:1.2em; font:12px Menlo, Monaco, Consolas, monospace; word-wrap: break-word; white-space: pre-wrap; position:relative; z-index:99999; word-break: normal',
        'num' => 'color:#0000FF',
        'const' => 'font-weight:bold; color:#0000FF',
        'str' => 'font-weight:bold; color:#008000',
        'note' => 'color:#000',
        'ref' => 'color:#000',
        'public' => 'font-weight:bold; color:#660E7A',
        'protected' => 'font-weight:bold; color:#660E7A',
        'private' => 'font-weight:bold; color:#660E7A',
        'meta' => 'color:#B729D9',
        'key' => 'color:#008000',
        'index' => 'color:#1299DA',
    );
}
