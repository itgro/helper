<?
use Itgro\App;
use Itgro\Form;
use Itgro\Request\Request;

$mid = Request::get()->get('mid');
$module_id = "helper";

$rights = App::getInstance()->GetGroupRight($module_id);

if (Request::is_post() && Request::post()->has('Update'))
{
	if (Request::post()->has('use_xdebug'))
	{
		COption::SetOptionInt('helper', 'use_xdebug', 1);
	}
	else
	{
		COption::SetOptionInt('helper', 'use_xdebug', 0);
	}

	if (Request::post()->has('xhprof_profiling'))
	{
		COption::SetOptionInt('helper', 'xhprof_profiling', 1);
	}
	else
	{
		COption::SetOptionInt('helper', 'xhprof_profiling', 0);
	}

	if (Request::post()->has('dump_depth'))
	{
		COption::SetOptionInt('helper', 'dump_depth', Request::post()->get('dump_depth'));
	}

	if (Request::post()->has('dump_length'))
	{
		COption::SetOptionInt('helper', 'dump_length', Request::post()->get('dump_length'));
	}

	if (Request::post()->has('dump_children'))
	{
		COption::SetOptionInt('helper', 'dump_children', Request::post()->get('dump_children'));
	}

	if (Request::post()->has('detect_env'))
	{
		COption::SetOptionInt('helper', 'detect_env', Request::post()->get('detect_env'));
	}

	if (Request::post()->has('environment'))
	{
		COption::SetOptionString('helper', 'environment', Request::post()->get('environment'));
	}

	if (Request::post()->has('dev_groups'))
	{
		$value = Request::post()->get('dev_groups');
		COption::SetOptionString('helper', 'dev_groups', json_encode($value));
	}
	else
	{
		COption::SetOptionString('helper', 'dev_groups', '[]');
	}
}

$xdebug_loaded = extension_loaded('xdebug');
$var_dump_overloaded = ini_get('xdebug.overload_var_dump') > 0;

$use_xdebug = COption::GetOptionInt('helper', 'use_xdebug', 0);
$dump_depth = COption::GetOptionInt('helper', 'dump_depth', 128);
$dump_length = COption::GetOptionInt('helper', 'dump_length', 512);
$dump_children = COption::GetOptionInt('helper', 'dump_children', 128);
$detect_env = COption::GetOptionInt('helper', 'detect_env', 1);
$environment = COption::GetOptionString('helper', 'environment', 'production');
$xhprof_profiling = COption::GetOptionInt('helper', 'xhprof_profiling', 1);

$dev_groups = COption::GetOptionString('helper', 'dev_groups', '[1]');
$dev_groups = json_decode($dev_groups, true);

if (null === $dev_groups)
{
	$dev_groups = [];
}

$dbUserGroups = CGroup::GetList($by, $order);

$arUserGroups = [];

while ($arUserGroup = $dbUserGroups->Fetch())
{
	$arUserGroups[$arUserGroup['ID']] = $arUserGroup['NAME'];
}

if ($rights >= "R")
{
	$aTabs = [
		[
			"DIV" => "main",
			"TAB" => "Главные",
			"ICON" => "",
			"TITLE" => "Основные настройки"
		],
		[
			"DIV" => "debug",
			"TAB" => "Отладка",
			"ICON" => "",
			"TITLE" => "Настройки отладки"
		],
		[
			"DIV" => "env",
			"TAB" => "Окружение",
			"ICON" => "",
			"TITLE" => "Настройки окружения"
		],
	];

	$tabControl = new CAdminTabControl("tabControl", $aTabs);
	$tabControl->Begin();

	Form::start(App::getInstance()->GetCurPage().'?mid='.htmlspecialcharsbx($mid).'&lang='.LANGUAGE_ID);

	echo bitrix_sessid_post();

	#region Основные
	$tabControl->BeginNextTab();
	?>
	<tr>
		<td width="40%" class="adm-detail-content-cell-l">
			Включить профилирование facebook/xhprof
		</td>
		<td width="60%" class="adm-detail-content-cell-r">
			<?
			Form::checkbox('xhprof_profiling', null, $xhprof_profiling);
			?>
		</td>
	</tr>
	<?
	$tabControl->EndTab();
	#endregion

	#region Отладка
	$tabControl->BeginNextTab();
	?>

	<tr>
		<td width="40%" class="adm-detail-content-cell-l">
			Использовать var_dump xdebug:
		</td>
		<td width="60%" class="adm-detail-content-cell-r">
			<?
			if ($xdebug_loaded && $var_dump_overloaded)
			{
				Form::checkbox('use_xdebug', null, $use_xdebug);
			}
			else
			{
				Form::checkbox('use_xdebug', null, true, ['disabled' => 'disabled']);
			}
			?>
		</td>
	</tr>
    <tr>
        <td colspan="2" align="center">
            <div class="adm-info-message-wrap" align="center">
                <div class="adm-info-message">
                    <? if (!$xdebug_loaded): ?>
                        Модуль <b>xdebug</b> не установлен. Будет использоваться symfony\var-dumper.
                    <? elseif (!$var_dump_overloaded): ?>
                        Не включена опция <i>xdebug.overload_var_dump</i>. Будет использоваться symfony\var-dumper.
                    <? else: ?>
                        В качестве dump может быть использован перегруженный var_dump модуля xdebug.
                    <? endif; ?>
                </div>
            </div>
        </td>
    </tr>
	<tr>
		<td width="40%" class="adm-detail-content-cell-l">
			Максимальная вложенность dump:
		</td>
		<td width="60%" class="adm-detail-content-cell-r">
			<?
			Form::input('text', 'dump_depth', null, $dump_depth)?>
		</td>
	</tr>

	<tr>
		<td width="40%" class="adm-detail-content-cell-l">
			Максимальная длинна строки dump:
		</td>
		<td width="60%" class="adm-detail-content-cell-r">
			<?
			Form::input('text', 'dump_length', null, $dump_length)?>
		</td>
	</tr>

	<tr>
		<td width="40%" class="adm-detail-content-cell-l">
			Макс. количество дочерних элементов dump:
		</td>
		<td width="60%" class="adm-detail-content-cell-r">
			<?
			Form::input('text', 'dump_children', null, $dump_children)?>
		</td>
	</tr>

	<?
	$tabControl->EndTab();
	#endregion

	#region Окружение
	$tabControl->BeginNextTab();
	?>

	<tr>
		<td width="40%" class="adm-detail-content-cell-l">
			Определять окружение:
		</td>
		<td width="60%" class="adm-detail-content-cell-r">
			<?
			Form::select(
				'detect_env',
				[
					'1' => 'Из настроек сервера',
					'2' => 'По пользователю',
					'3' => 'Вручную'
				],
				$detect_env,
				1
			);?>
		</td>
	</tr>

	<tr>
		<td width="40%" class="adm-detail-content-cell-l">
			Группы разработчиков:
		</td>
		<td width="60%" class="adm-detail-content-cell-r">
			<?
			Form::select(
				'dev_groups[]',
				$arUserGroups,
				$dev_groups,
				null,
				true
			);?>
		</td>
	</tr>

	<tr>
		<td width="40%" class="adm-detail-content-cell-l">
			Окружение вручную:
		</td>
		<td width="60%" class="adm-detail-content-cell-r">
			<?
			Form::input(
				'text',
				'environment',
				$environment,
				'production'
			);?>
		</td>
	</tr>

	<?
	$tabControl->EndTab();
	#endregion

	$tabControl->Buttons();

	Form::input('submit', 'Update', GetMessage("MAIN_APPLY"), null, ['class' => 'adm-btn-save']);
	Form::input('hidden', 'Update', 'Y');
	Form::input('reset', 'reset', GetMessage("MAIN_RESET"));

	$tabControl->End();

	Form::end();
}
